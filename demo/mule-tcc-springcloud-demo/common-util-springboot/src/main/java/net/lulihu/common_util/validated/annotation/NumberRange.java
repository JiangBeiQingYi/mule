package net.lulihu.common_util.validated.annotation;

import net.lulihu.common_util.validated.spring.NumberRangeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 数值类型取值范围
 * <p>
 * 与注解指定的对象的数据类型进行比较匹配
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = NumberRangeValidator.class)
public @interface NumberRange {

    short[] SHORTS() default {};

    byte[] BYTES() default {};

    int[] INTS() default {};

    long[] LONGS() default {};

    double[] DOUBLES() default {};

    float[] FLOATS() default {};

    /**
     * 抛出参数异常，异常信息
     * <p>
     * 空或者null 则抛出默认异常信息
     */
    String message() default "超出数值范围";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
