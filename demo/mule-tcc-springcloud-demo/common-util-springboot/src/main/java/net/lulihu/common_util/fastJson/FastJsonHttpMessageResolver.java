package net.lulihu.common_util.fastJson;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.BeanKit;
import net.lulihu.ObjectKit.ClassKit;
import net.lulihu.ObjectKit.MapKit;
import net.lulihu.ObjectKit.ObjectKit;
import net.lulihu.functional.FieldValueProvider;
import net.lulihu.http.RequestExpandWrapperUtil;
import org.springframework.core.MethodParameter;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 使用fastJson作为请求参数解析器
 */
@Slf4j
public class FastJsonHttpMessageResolver implements HandlerMethodArgumentResolver {

    public FastJsonHttpMessageResolver() {
        log.info("==== 加载FastJsonHttpMessageResolver组件 ====");
    }

    /**
     * 只有使用 @FastJson 注解的参数才进行参数解析
     * <p>
     * fastJson注解 只支持JavaBean对象
     */
    @Override
    public boolean supportsParameter(@NonNull MethodParameter parameter) {
        Class<?> parameterType = parameter.getParameterType();
        return parameter.hasParameterAnnotation(FastJson.class)
                && (BeanKit.isBean(parameterType) || ClassKit.isAssignable(List.class, parameterType));
    }

    @Override
    public Object resolveArgument(@NonNull MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  @NonNull NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
        HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
        Class<?> parameterType = parameter.getParameterType();
        // bean对象
        if (BeanKit.isBean(parameterType)) {
            Map<String, Object> requestParameters = RequestExpandWrapperUtil.requestParams(servletRequest, new HashMap<>());
            if (MapKit.isEmpty(requestParameters)) return ClassKit.newInstanceConstructorsDefaultValue(parameterType);

            try {
                String json = JSON.toJSONString(requestParameters);
                return JSON.parseObject(json, parameterType);
                // 兼容模式
            } catch (JSONException e) {
                return BeanKit.fillBeanField(ClassKit.newInstance(parameterType), (FieldValueProvider) (field) -> {
                    Object value = requestParameters.get(field.getName());
                    if (ObjectKit.hasEmpty(value)) return null;
                    // 不匹配时
                    if (!value.getClass().equals(field.getType()))
                        return distribute(value, field);
                    return value;
                });
            }
            //集合对象
        } else if (ClassKit.isAssignable(List.class, parameterType)) {
            Type genericType = parameter.getParameter().getParameterizedType();
            if (genericType instanceof ParameterizedType) {
                ParameterizedType pt = (ParameterizedType) genericType;
                return RequestExpandWrapperUtil.requestParams(servletRequest, (Class<?>) pt.getActualTypeArguments()[0]);
            }
        }
        throw new IllegalArgumentException("不支持的封装类型...");
    }

    /**
     * 处理数据分发
     *
     * @param value 值
     * @param field 字段属性
     */
    private Object distribute(Object value, Field field) {
        String val = value instanceof String ? (String) value : JSON.toJSONString(value);
        Class<?> clazz = field.getType();
        // 处理集合
        if (Collection.class.isAssignableFrom(clazz)) {
            return processCollection(val, field);
        }
        return JSON.parseObject(val, clazz);
    }

    private Object processCollection(String value, Field field) {
        Type genericType = field.getGenericType();
        // 如果是泛型参数的类型
        if (genericType instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) genericType;
            return JSON.parseArray(value, (Class<?>) pt.getActualTypeArguments()[0]);
        }
        return null;
    }
}
