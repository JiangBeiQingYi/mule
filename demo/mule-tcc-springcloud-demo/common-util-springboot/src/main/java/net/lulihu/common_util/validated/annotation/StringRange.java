package net.lulihu.common_util.validated.annotation;

import net.lulihu.common_util.validated.spring.StringRangeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 设置 字符串范围
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = StringRangeValidator.class)
public @interface StringRange {

    /**
     * 设置字符串选择范围，都不匹配时，抛出参数异常
     */
    String[] value() default {};

    /**
     * 最大字符串长度 默认为-1 不限制
     */
    int maxLen() default -1;

    /**
     * 最小字符串长度
     */
    int minLen() default -1;

    /**
     * 抛出参数异常，异常信息
     * <p>
     * 空或者null 则抛出默认异常信息
     */
    String message() default "超出字符取值范围";


    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
