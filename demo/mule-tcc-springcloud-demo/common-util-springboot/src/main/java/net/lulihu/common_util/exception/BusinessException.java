package net.lulihu.common_util.exception;

import net.lulihu.exception.ExceptionEnum;
import net.lulihu.ObjectKit.StrKit;

/**
 * 封装业务异常信息
 */
public class BusinessException extends RuntimeException implements ExceptionEnum {

    /**
     * 默认http 错误代码
     */
    private static final Integer defaultHttpCode = 555;

    /**
     * 默认自定义错误消息代码
     */
    private static final Integer defaultCode = 999;

    public BusinessException(ExceptionEnum exceptionEnum) {
        this(exceptionEnum.getHttpCode(), exceptionEnum.getCode(), exceptionEnum.getMessage());
    }

    public BusinessException(Throwable cause, ExceptionEnum exceptionEnum) {
        this(cause, exceptionEnum.getHttpCode(), exceptionEnum.getCode(), exceptionEnum.getMessage());
    }

    public BusinessException(String message) {
        this(null, defaultHttpCode, defaultCode, message);
    }

    public BusinessException(Integer code, String message) {
        this(null, defaultHttpCode, code, message);
    }

    public BusinessException(Integer httpCode, Integer code, String message) {
        this(null, httpCode, code, message);
    }

    public BusinessException(String message, Object... values) {
        this(null, defaultHttpCode, defaultCode, StrKit.format(message, values));
    }

    public BusinessException(Integer code, String message, Object... values) {
        this(null, defaultHttpCode, code, StrKit.format(message, values));
    }

    public BusinessException(Throwable cause, String message, Object... values) {
        this(cause, defaultHttpCode, defaultCode, StrKit.format(message, values));
    }

    public BusinessException(Throwable cause, Integer code, String message, Object... values) {
        this(cause, defaultHttpCode, code, StrKit.format(message, values));
    }

    public BusinessException(Integer httpCode, Integer code, String message, Object... values) {
        this(null, httpCode, code, StrKit.format(message, values));
    }

    public BusinessException(Throwable cause, Integer httpCode, Integer code, String message, Object... values) {
        this(cause, httpCode, code, StrKit.format(message, values));
    }

    private BusinessException(Throwable cause, Integer httpCode, Integer code, String message) {
        super(cause);
        this.httpCode = httpCode;
        this.code = code;
        this.message = message;
    }

    private Integer code;

    private Integer httpCode;

    private String message;

    public Integer getHttpCode() {
        return httpCode;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
