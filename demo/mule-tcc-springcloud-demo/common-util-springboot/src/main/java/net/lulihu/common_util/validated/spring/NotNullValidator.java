package net.lulihu.common_util.validated.spring;

import net.lulihu.common_util.validated.annotation.NotNull;

import javax.validation.ConstraintValidator;

/**
 * 时间格式校验
 */
public class NotNullValidator extends Validator<Object> implements ConstraintValidator<NotNull, Object> {

    public void initialize(NotNull annotation) {
        setAnnotation(annotation);
    }

}
