package net.lulihu.common_util.validated.annotation;

import net.lulihu.common_util.validated.spring.DateStrFormatValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 强制约束字符串时间类型
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = DateStrFormatValidator.class)
public @interface DateStrFormat {

    String value();

    /**
     * 抛出参数异常，异常信息
     * <p>
     * 如果为空则不验证参数
     */
    String message() default "时间格式错误";

    /**
     * 是否为复数
     */
    boolean plural() default false;

    /**
     * 为多个时，分隔符
     */
    String pluralSep() default ",";


    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
