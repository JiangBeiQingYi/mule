package net.lulihu.common_util.spring;

import net.lulihu.common_util.fastJson.FastJson;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.HttpKit;
import net.lulihu.ObjectKit.ObjectKit;
import net.lulihu.http.RequestExpandWrapperUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Objects;

/**
 * 基于spring封装的常用http工具
 */
@Slf4j
public class SpringHttpKit {

    /**
     * 获取 springMvc 当前的 HttpServletRequest 对象
     */
    public static HttpServletResponse getResponse() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (null == servletRequestAttributes) return null;
        return servletRequestAttributes.getResponse();
    }

    /**
     * 获取springMvc 当前的 HttpServletRequest 对象
     */
    public static HttpServletRequest getRequest() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (null == servletRequestAttributes) return null;
        return HttpKit.getRequest(servletRequestAttributes.getRequest());
    }


    /**
     * 获取指定请求头
     *
     * @param key 键
     * @return 根据键获取对应的值
     */
    public static String getHeader(String key) {
        return Objects.requireNonNull(getRequest()).getHeader(key);
    }


    /**
     * 获取所有 头部参数
     */
    public static Map<String, Object> getHeaderParam() {
        return HttpKit.getHeaderParam(getRequest());
    }

    /**
     * 获取所有请求参数
     *
     * @param request 请求
     * @param handler 处理器
     * @param result  返回值
     * @return 返回值
     */
    public static Map<String, Object> getAllRequestParams(HttpServletRequest request,
                                                          Object handler,
                                                          Map<String, Object> result) {
        HandlerMethod handlerMethod = null;
        if (handler instanceof HandlerMethod)
            handlerMethod = (HandlerMethod) handler;
        else
            log.warn("spring 方法处理对象发生改变");
        return getAllRequestParams(request, handlerMethod, result);
    }


    /**
     * 获取所有请求参数 包括请求流中
     * 该方法只限于 spring 框架
     *
     * @param request       请求对象
     * @param handlerMethod 处理方法对象
     */
    public static Map<String, Object> getAllRequestParams(HttpServletRequest request,
                                                          HandlerMethod handlerMethod,
                                                          Map<String, Object> result) {
        if (ObjectKit.hasNotEmpty(handlerMethod)) {
            for (Annotation[] parameterAnnotation : handlerMethod.getMethod().getParameterAnnotations()) {
                for (Annotation annotation : parameterAnnotation) {
                    if (annotation instanceof RequestBody || annotation instanceof FastJson) {
                        RequestExpandWrapperUtil.requestParams(request, result);
                        break;
                    }
                }
            }
        } else
            HttpKit.getRequestParameters(request, result);
        return result;
    }


    /**
     * 获取客户端真实ip
     */
    public static String getClientIP() {
        return HttpKit.getClientIP(getRequest());
    }

}
