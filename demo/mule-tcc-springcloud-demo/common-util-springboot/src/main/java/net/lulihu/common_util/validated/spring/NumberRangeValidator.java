package net.lulihu.common_util.validated.spring;

import net.lulihu.common_util.validated.annotation.NumberRange;

import javax.validation.ConstraintValidator;

/**
 * 整形范围校验
 */
public class NumberRangeValidator extends Validator<Number> implements ConstraintValidator<NumberRange, Number> {

    public void initialize(NumberRange annotation) {
        setAnnotation(annotation);
    }
}
