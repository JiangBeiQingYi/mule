package net.lulihu.common_util.exception;

import net.lulihu.exception.ExceptionEnum;

/**
 * 常规业务异常枚举
 * <p>
 * 从错误代码1000开始，依次递增
 * <p>
 * http代码将 https://tools.ietf.org/html/rfc7231#section-6.5.8
 * <p>
 * 自定义错误http code 555
 * 自定义错误消息code  999
 */
public enum BusinessExceptionEnum implements ExceptionEnum {

    /* ******** 错误异常 ********  */
    EXPECTED_TIMED_ERROR(408, 1000, "预期之外的超时响应，请刷新后重试！"),
    EXPECTED_ERROR(500, 1001, "预期之外的错误，请稍后重试！"),
    ILLEGAL_OPERATION(406, 1002, "预期之外的操作，无法执行!"),
    SERVICE_IS_NOT_AVAILABLE(503, 1019, "服务不可用，请稍后重试"),

    /* ******** 错误的请求 ********  */
    REQUEST_NULL(400, 1003, "无法解析请求,请检查参数!"),
    INVALID_DATE_STRING(400, 1004, "日期格式无法匹配，请检查!"),
    PARAM_NOT_REQUIRE(400, 1005, "参数不合法,请检查后重试!"),
    HTTP_MESSAGE_NOT_READABLE(400, 1006, "HTTP消息不可读"),
    NO_FOUND(404, 1007, "请求地址不存在"),

    /* ******** 访问错误 ********  */
    NO_ACCESS(403, 1008, "无访问权限"),
    NO_PLATFORM_ACCESS(403, 1009, "无平台访问权限"),
    NO_LONGER_USE(410, 1010, "接口已经停止使用，请联系管理员获取最新接口信息"),
    NOT_ONLINE(502, 1011, "接口暂未上线，请及时关注最新文档"),
    SECRET_PROHIBITED_AS_A_REQUEST_PARAMETER(400, 1012, "秘钥禁止作为请求参数"),
    DO_NOT_ALLOW_DUPLICATE_REQUESTS(403, 1013, "禁止重复请求"),
    REQUEST_TIMED_OUT(400, 1014, "请求时间超出约束"),
    PROHIBIT_ILLEGAL_ACCESS(403, 1015, "禁止非法访问"),
    TOKEN_HAS_EXPIRED(403, 1016, "令牌已过期"),
    THE_NUMBER_OF_VISITS_EXCEEDED(413, 1020, "当前访问人数众多，请稍后重试"),

    /* ******** 资源问题 ********  */
    RECORD_DOES_NOT_EXIST(555, 1017, "记录不存在"),
    RESOURCE_EXISTS_CANNOT_RECREATED(555, 1018, "资源已经存在且禁止重复创建/修改"),
    ;


    BusinessExceptionEnum(Integer httpCode, Integer code, String message) {
        this.httpCode = httpCode;
        this.code = code;
        this.message = message;
    }

    private Integer httpCode;

    private Integer code;

    private String message;

    public Integer getHttpCode() {
        return httpCode;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
