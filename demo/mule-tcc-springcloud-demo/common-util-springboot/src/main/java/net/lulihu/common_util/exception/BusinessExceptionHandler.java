package net.lulihu.common_util.exception;

import net.lulihu.common_util.controller_result.Result;
import net.lulihu.common_util.spring.SpringHttpKit;
import com.netflix.client.ClientException;
import feign.RetryableException;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.StrKit;
import net.lulihu.common_util.validated.spring.ParamViolationExceptionUtil;
import net.lulihu.exception.ExceptionEnum;
import net.lulihu.exception.ParamResolveException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;
import java.util.List;

/**
 * 全局的的异常拦截器（拦截所有的控制器）
 */
@Slf4j
@ResponseBody
public abstract class BusinessExceptionHandler {

    /**
     * 拦截业务异常,异常在控制范围内
     */
    @ExceptionHandler(BusinessException.class)
    public Object business(BusinessException e) {
        log.warn(StrKit.format("{}-{}", e.getCode(), e.getMessage()), e);
        return unifiedReturn(e);
    }

    /**
     * 服务不可用
     */
    @ExceptionHandler({ClientException.class})
    public Object serviceIsNotAvailable(ClientException e) {
        log.warn("", e);
        return unifiedReturn(BusinessExceptionEnum.SERVICE_IS_NOT_AVAILABLE);
    }

    /**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    public Object unknown(RuntimeException e) {
        log.error("运行时未知异常:", e);
        return unifiedReturn(BusinessExceptionEnum.EXPECTED_ERROR);
    }

    /**
     * 参数类型不匹配异常
     */
    @ExceptionHandler({MethodArgumentTypeMismatchException.class, ParamResolveException.class, ConstraintViolationException.class})
    public Object argumentTypeMismatch(Exception e) {
        if (e instanceof ConstraintViolationException) {
            List<String> messages = ParamViolationExceptionUtil.getParamViolationMessages((ConstraintViolationException) e);
            log.warn("请求参数不合法例外:{}", messages);
            return unifiedReturn(BusinessExceptionEnum.PARAM_NOT_REQUIRE, messages);
        }
        log.warn("参数解析例外:{}", e.getMessage());
        return unifiedReturn(BusinessExceptionEnum.PARAM_NOT_REQUIRE);
    }

    /**
     * http请求 缺少必要的请求正文
     */
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public Object httpMessageNotReadable(HttpMessageNotReadableException e) {
        log.warn("缺少必要的http请求正文", e);
        return unifiedReturn(BusinessExceptionEnum.HTTP_MESSAGE_NOT_READABLE);
    }

    /**
     * feign 请求超时
     */
    @ExceptionHandler({RetryableException.class})
    public Object nestedServletException(RetryableException e) {
        log.warn("", e);
        return unifiedReturn(BusinessExceptionEnum.EXPECTED_TIMED_ERROR);
    }

    public Object unifiedReturn(ExceptionEnum exceptionEnum) {
        return unifiedReturn(exceptionEnum, null);
    }

    public Object unifiedReturn(ExceptionEnum exceptionEnum, Object data) {
        Integer httpCode = exceptionEnum.getHttpCode();
        if (null != httpCode) SpringHttpKit.getResponse().setStatus(httpCode);

        return Result.exception(exceptionEnum, data);
    }

}
