package net.lulihu.mule.tccSpringcloud.demo.inventory.model.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class Product {

    /**
     * 商品编号
     */
    private String productNumber;

    /**
     * 商品数量
     */
    private Integer quantity;

}