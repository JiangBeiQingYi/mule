package net.lulihu.mule.tccSpringcloud.demo.inventory.service.impl;

import net.lulihu.mule.tccSpringcloud.demo.inventory.service.InventoryService;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import org.springframework.stereotype.Service;

@Service
public class InventoryServiceImpl implements InventoryService {

    @MuleTcc(confirmMethod = "getInventoryConfirmMethod", cancelMethod = "getInventoryCancelMethod")
    @Override
    public Object getInventory() {
        return 20;
    }


    public void getInventoryConfirmMethod() {
        System.out.println("执行成功");
    }

    public void getInventoryCancelMethod() {
        System.out.println("取消回滚");
    }

}
