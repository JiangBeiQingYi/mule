package net.lulihu.mule.tccSpringcloud.demo.inventory.controller;

import net.lulihu.common_util.AbstractController;
import net.lulihu.common_util.controller_result.Result;
import net.lulihu.common_util.fastJson.FastJson;
import net.lulihu.mule.tccSpringcloud.demo.inventory.model.vo.Product;
import net.lulihu.mule.tccSpringcloud.demo.inventory.service.PressureTestService;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PressureTestController extends AbstractController {

    @Autowired
    private PressureTestService pressureTestService;

    /**
     * 获取商品列表
     */
    @RequestMapping("getProductList")
    public Result getProductList() {
        return Result.success(pressureTestService.getProductList());
    }

    /**
     * 获取商品库存
     *
     * @param products 商品
     * @return 返回商品信息
     */
    @MuleTcc(confirmMethod = "confirmGetProductInventory", cancelMethod = "cancelGetProductInventory")
    @RequestMapping("getProductInventory")
    public Result getProductInventory(@FastJson List<Product> products) {
        return Result.success(pressureTestService.getProductInventory(products));
    }

    public void cancelGetProductInventory(List<Product> products) {
        System.out.println("取消方法");
        System.out.println(products);
    }

    public void confirmGetProductInventory(List<Product> products) {
        System.out.println("确认方法");
        System.out.println(products);
    }
}
