package net.lulihu.mule.tccSpringcloud.demo.inventory.service;

public interface InventoryService {

    Object getInventory();

}
