package net.lulihu.mule.tcc.pressureTest;

import com.alibaba.fastjson.JSON;
import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import net.lulihu.ObjectKit.NumberKit;
import net.lulihu.common_util.controller_result.Result;
import net.lulihu.designPattern.chain.AbstractHandlerResponsibilityChainResolver;
import net.lulihu.designPattern.chain.ResponsibilityChainHandler;
import net.lulihu.disruptorKit.DefaultEventFactory;
import net.lulihu.disruptorKit.DisruptorManage;
import net.lulihu.disruptorKit.Event;
import net.lulihu.disruptorKit.Producer;
import net.lulihu.disruptorKit.oneOf.WorkHandlerManage;
import net.lulihu.http.okhttp.HttpUtils;
import net.lulihu.mule.tcc.pressureTest.model.CreateOrder;
import net.lulihu.mule.tcc.pressureTest.model.Inventory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.stereotype.Component;

import java.util.*;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@Component("net.lulihu.mule.tcc.pressureTest")
public class PressureTestApplication {

    public static void main(String[] args) {
//        SpringApplication.run(PressureTestApplication.class, args);

        Producer<Object> producer = createPool(4);
        try { // 休眠一秒让异步线程池正式启动
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


//        for (int i = 0; i < 10; i++) {
            List<Inventory> productList = getProductList();
            List<CreateOrder> orders = randomSelectProducts(productList);

            for (CreateOrder order : orders) {
                producer.submit(order);
            }
//        }
    }

    /**
     * 创建订单
     */
    public static void createOrder(CreateOrder createOrder) {
        HttpUtils.sendPOSTJson("http://localhost:10112/createOrder", null, JSON.toJSONString(createOrder), Result.class);
    }

    /**
     * 获取商品列表
     */
    public static List<Inventory> getProductList() {
        Result result = HttpUtils.sendGET("http://localhost:10113/getProductList", null, null, Result.class);
        return result.getDataList(Inventory.class);
    }

    private static final int[] productCount = new int[]{1, 10}; // 商品数量取值范围

    private static final int[] orderProducts = new int[]{1, 3}; // 每个订单不同商品的数量取值范围

    private static final int orderCount = 100; // 生成订单数量

    public static List<CreateOrder> randomSelectProducts(List<Inventory> products) {
        List<CreateOrder> orders = new ArrayList<>(orderCount);
        for (int i = 0; i < orderCount; i++) {
            List<CreateOrder.Product> productList = new ArrayList<>();
            // 当前订单商品数量
            int orderProductCount = NumberKit.rangeRandomNumber(orderProducts[0], orderProducts[1]);

            Set<Integer> inxTmp = new HashSet<>();
            while (true) {
                // 随机选择商品  同一个订单内不能出现重复的商品
                int inx;
                while (true) {
                    inx = NumberKit.rangeRandomNumber(0, products.size() - 1);
                    if (!inxTmp.contains(inx)) {
                        inxTmp.add(inx);
                        break;
                    }
                }

                Inventory inventory = products.get(inx);
                // 随机选择数量
                int count = NumberKit.rangeRandomNumber(productCount[0], productCount[1]);
                Integer quantity = inventory.getQuantity();
                if (quantity == 0) break;

                if (quantity < count) {
                    count = NumberKit.rangeRandomNumber(productCount[0], quantity);
                }
                inventory.setQuantity(quantity - count);

                CreateOrder.Product product = new CreateOrder.Product();
                product.setProductNumber(inventory.getProductNumber());
                product.setQuantity(count);
                productList.add(product);
                // 达到数量停止
                if (productList.size() == orderProductCount) break;
            }
            CreateOrder createOrder = new CreateOrder();
            createOrder.setProducts(productList);
            createOrder.setUserId(10);
            orders.add(createOrder);
        }
        return orders;
    }

    /**
     * 创建线程池
     *
     * @param concurrent 并发线程数量
     * @return 生产者对象
     */
    public static Producer<Object> createPool(int concurrent) {
        // 执行者
        ResponsibilityChainHandler<CreateOrder> createOrderHandler = new ResponsibilityChainHandler<>();
        createOrderHandler.addHandler(new AbstractHandlerResponsibilityChainResolver<CreateOrder>("测试") {
            @Override
            public void resolve(CreateOrder createOrder) {
                try {
                    createOrder(createOrder);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // 注册责任链
        WorkHandlerManage handlerManage = WorkHandlerManage.getInstance();
        handlerManage.addWorkHandler(CreateOrder.class, createOrderHandler);

        //注册 Disruptor
        DisruptorManage instance = DisruptorManage.getInstance();
        EventFactory<Event<Object>> eventFactory = DefaultEventFactory.factory();

        String name = "压力测试";
        Disruptor<Event<Object>> eventDisruptor =
                instance.registered(name, eventFactory, 1024, ProducerType.SINGLE, new BlockingWaitStrategy());
        // 定义消费者  不重复消费
        eventDisruptor.handleEventsWithWorkerPool(handlerManage.consumerNum(concurrent));
        // 启动
        eventDisruptor.start();

        // 生产者对象
        return new Producer<>(eventDisruptor.getRingBuffer());
    }


}
