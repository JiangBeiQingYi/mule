package net.lulihu.mule.tccSpringcloud.demo.order.controller;

import net.lulihu.common_util.AbstractController;
import net.lulihu.common_util.controller_result.Result;
import net.lulihu.mule.tccSpringcloud.demo.order.model.vo.CreateOrder;
import net.lulihu.mule.tccSpringcloud.demo.order.service.PressureTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 压力测试控制器
 */
@RestController
public class PressureTestController extends AbstractController {

    @Autowired
    private PressureTestService pressureTestService;

    /**
     * 创建订单
     *
     * @param order 产品
     * @return 返回订单编号
     */
    @RequestMapping("createOrder")
    public Result createOrder(@RequestBody CreateOrder order) {
        return Result.success().put("orderNumber", pressureTestService.createOrder(order));
    }

}
