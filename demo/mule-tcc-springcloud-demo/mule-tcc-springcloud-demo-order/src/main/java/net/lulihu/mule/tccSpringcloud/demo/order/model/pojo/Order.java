package net.lulihu.mule.tccSpringcloud.demo.order.model.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Order {

    /**
     * 订单编号
     */
    private String orderNumber;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 总价
     */
    private Double totalPrice;

}
