package net.lulihu.mule.tccSpringcloud.demo.order.client;

import net.lulihu.common_util.controller_result.Result;
import net.lulihu.mule.tccSpringcloud.demo.order.model.vo.CreateOrder;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient("mule-tcc-springcloud-demo-inventory")
public interface InventoryClient {

    @MuleTcc(currentMethod = true)
    @RequestMapping("getInventory")
    Result getInventory();

    @MuleTcc(currentMethod = true)
    @RequestMapping("getProductInventory")
    Result getProductInventory(@RequestBody List<CreateOrder.Product> products);


}
