package net.lulihu.mule.tccSpringcloud.demo.order.model.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 购买产品
 */
@Data
@NoArgsConstructor
public class CreateOrder {

    /**
     * 商品集
     */
    private List<Product> products;

    /**
     * 用户id
     */
    private Integer userId;

    @Data
    @NoArgsConstructor
    public static class Product {

        /**
         * 商品编号
         */
        private String productNumber;

        /**
         * 商品数量
         */
        private Integer quantity;

    }
}
