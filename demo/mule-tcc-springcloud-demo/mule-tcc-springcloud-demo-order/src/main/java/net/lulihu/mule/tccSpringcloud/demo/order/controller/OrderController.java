package net.lulihu.mule.tccSpringcloud.demo.order.controller;

import net.lulihu.common_util.AbstractController;
import net.lulihu.mule.tccSpringcloud.demo.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController extends AbstractController {


    @Autowired
    private OrderService orderService;

    /**
     * 能否购买
     *
     * @param number    数量
     * @param unitPrice 单价
     * @return true为可以反之不可以
     */
    @RequestMapping("detriment")
    public Object detriment(Integer number, Integer unitPrice) {
        number = 10;
        unitPrice = 5;
        return orderService.detriment(number, unitPrice);
    }

}
