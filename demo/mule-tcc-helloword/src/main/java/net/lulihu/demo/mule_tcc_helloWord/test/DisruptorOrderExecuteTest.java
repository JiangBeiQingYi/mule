package net.lulihu.demo.mule_tcc_helloWord.test;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import net.lulihu.designPattern.chain.AbstractHandlerResponsibilityChainResolver;
import net.lulihu.designPattern.chain.ResponsibilityChainHandler;
import net.lulihu.disruptorKit.DefaultEventFactory;
import net.lulihu.disruptorKit.DisruptorManage;
import net.lulihu.disruptorKit.Event;
import net.lulihu.disruptorKit.Producer;
import net.lulihu.disruptorKit.oneOf.WorkHandlerManage;
import net.lulihu.lock.ConditionLock;
import net.lulihu.lock.OrderExecuteLockKit;

import java.util.ArrayList;
import java.util.List;

public class DisruptorOrderExecuteTest {

    public static void main(String[] args) {
        Producer<Object> producer = createPool(1);

        List<ListObj> objss = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            List<Obj> objs = new ArrayList<>();
            OrderExecuteLockKit executeLockKit = new OrderExecuteLockKit();
            for (int j = 1; j < 10; j++) {
                objs.add(new Obj(executeLockKit.getConditionLock(), i + " ->" + j));
            }
            objss.add(new ListObj(objs, producer));
        }

        for (ListObj listObj : objss) {
            producer.submit(listObj);
        }


    }

    public static class ListObj {

        private List<Obj> objs;

        private Producer<Object> producer;

        public ListObj(List<Obj> objs, Producer<Object> producer) {
            this.objs = objs;
            this.producer = producer;
        }

        public List<Obj> getObjs() {
            return objs;
        }

        public void setObjs(List<Obj> objs) {
            this.objs = objs;
        }

        public Producer<Object> getProducer() {
            return producer;
        }

        public void setProducer(Producer<Object> producer) {
            this.producer = producer;
        }
    }

    public static class Obj {
        private final ConditionLock conditionLockKit;

        private final String mes;

        public Obj(ConditionLock conditionLockKit, String mes) {
            this.conditionLockKit = conditionLockKit;
            this.mes = mes;
        }

        public ConditionLock getConditionLockKit() {
            return conditionLockKit;
        }

        public String getMes() {
            return mes;
        }
    }

    /**
     * 创建线程池
     *
     * @param concurrent 并发线程数量
     * @return 生产者对象
     */
    public static Producer<Object> createPool(int concurrent) {
        // 执行者
        ResponsibilityChainHandler<Obj> outHandler = new ResponsibilityChainHandler<>();
        outHandler.addHandler(new AbstractHandlerResponsibilityChainResolver<Obj>("测试") {
            @Override
            public void resolve(Obj obj) {
                ConditionLock conditionLockKit = obj.getConditionLockKit();
                try {
                    conditionLockKit.getLock();
                    System.out.println(obj.getMes());
                } finally {
                    conditionLockKit.unlock();
                }
            }
        });

        // 执行者
        ResponsibilityChainHandler<ListObj> addHandler = new ResponsibilityChainHandler<>();
        addHandler.addHandler(new AbstractHandlerResponsibilityChainResolver<ListObj>("测试") {
            @Override
            public void resolve(ListObj objs) {
                for (Obj obj : objs.getObjs()) {
                    objs.getProducer().submit(obj);
                }
            }
        });

        // 注册责任链
        WorkHandlerManage handlerManage = WorkHandlerManage.getInstance();
        handlerManage.addWorkHandler(Obj.class, outHandler);
        handlerManage.addWorkHandler(ListObj.class, addHandler);


        //注册 Disruptor
        DisruptorManage instance = DisruptorManage.getInstance();
        EventFactory<Event<Object>> eventFactory = DefaultEventFactory.factory();

        String name = "测试按序执行锁";
        Disruptor<Event<Object>> eventDisruptor =
                instance.registered(name, eventFactory, 1024, ProducerType.MULTI, new BlockingWaitStrategy());
        // 定义消费者  不重复消费
        eventDisruptor.handleEventsWithWorkerPool(handlerManage.consumerNum(concurrent));
        // 启动
        eventDisruptor.start();

        // 生产者对象
        return new Producer<>(eventDisruptor.getRingBuffer());
    }

}
