package net.lulihu.mule.tccSpringcloud;

import feign.RequestInterceptor;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.mule.tccTransaction.service.MuleTccBootService;
import net.lulihu.mule.tccTransaction.service.TransactionCoordinatorService;
import net.lulihu.mule.tccTransaction.service.TransactionMethodProxyService;
import net.lulihu.mule.tccTransaction.service.coordinator.DefaultTransactionCoordinatorServiceImpl;
import net.lulihu.mule.tccTransaction.service.impl.DefaultMuleTccBootServiceImpl;
import net.lulihu.mule.tccTransaction.service.impl.DefaultTransactionMethodProxyServiceImpl;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Mule Tcc SpringCloud配置中心
 */
@Slf4j
@Configuration
@EnableConfigurationProperties
public class MuleTccSpringCloudConfiguration {

    public MuleTccSpringCloudConfiguration() {
        LogKit.debug(log, "Mule Tcc Spring Cloud 自动配置...");
    }

    /**
     * Mule Tcc SpringCloud 启动应用程序
     */
    @Bean
    public MuleTccSpringCloudBootApplication muleTccSpringCloudBootApplication() {
        return new MuleTccSpringCloudBootApplication();
    }

    /**
     * 请求过滤器
     */
    @Bean
    public MuleTccHttpServletRequestHandlerFilter muleTccHttpServletRequestHandlerFilter(
            MuleTccConfigProperties muleTccConfigProperties) {
        return new MuleTccHttpServletRequestHandlerFilter(muleTccConfigProperties);
    }

    /**
     * AspectJ 代理
     */
    @Bean
    public MuleTccTransactionAspectJ muleTccTransactionAspectJ() {
        return new MuleTccTransactionAspectJ();
    }

    /**
     * 事务协调员组件
     */
    @Bean
    public TransactionCoordinatorService muleTccTransactionCoordinatorService() {
        return new DefaultTransactionCoordinatorServiceImpl();
    }

    /**
     * Mule Tcc启动服务
     *
     * @param transactionCoordinatorService 事务协调员
     */
    @Bean
    public MuleTccBootService muleTccBootService(TransactionCoordinatorService transactionCoordinatorService) {
        return new DefaultMuleTccBootServiceImpl(transactionCoordinatorService);
    }

    /**
     * 事务代理方法服务
     */
    @Bean
    public TransactionMethodProxyService muleTccTransactionMethodProxyService() {
        return new DefaultTransactionMethodProxyServiceImpl();
    }

    /**
     * ioc容器bean加载处理器
     *
     * @param transactionMethodProxyService 事务代理方法服务
     */
    @Bean
    public BeanPostProcessor muleTccBeanPostProcessor(TransactionMethodProxyService transactionMethodProxyService) {
        return new MuleTccBeanPostProcessor(transactionMethodProxyService);
    }

    /**
     * feign 请求拦截器
     */
    @Bean
    public RequestInterceptor muleTccFeignRequestInterceptor() {
        return new MuleTccFeignRequestInterceptor();
    }

    /**
     * 应用上下文持有者
     */
    @Bean
    public ApplicationContextHolder muleApplicationContextHolder() {
        return new ApplicationContextHolder();
    }
}
