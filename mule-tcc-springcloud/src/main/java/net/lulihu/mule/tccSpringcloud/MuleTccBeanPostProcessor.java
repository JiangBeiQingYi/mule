package net.lulihu.mule.tccSpringcloud;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.StrKit;
import net.lulihu.mule.tccTransaction.MuleTccProxyInvocationHandler;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import net.lulihu.mule.tccTransaction.service.TransactionMethodProxyService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanCurrentlyInCreationException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.lang.NonNull;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


/**
 * 在spring ioc容器加载bean之后判断进行对象代理
 */
@Slf4j
public class MuleTccBeanPostProcessor implements BeanPostProcessor {

    private final TransactionMethodProxyService methodProxyService;

    public MuleTccBeanPostProcessor(TransactionMethodProxyService methodProxyService) {
        this.methodProxyService = methodProxyService;
    }

    @Override
    public Object postProcessAfterInitialization(@NonNull Object bean, String beanName) throws BeansException {
        Class<?> aClass = bean.getClass();
        if (!Proxy.isProxyClass(aClass)) return bean;

        // 反射获取调用处理程序
        InvocationHandler handler = Proxy.getInvocationHandler(bean);
        // 获取该对象所有方法
        Method[] methods = ReflectionUtils.getAllDeclaredMethods(aClass);

        // 如果有一个对象使用事务注解则对该对象进行代理
        for (Method method : methods) {
            MuleTcc muleTcc = AnnotationUtils.findAnnotation(method, MuleTcc.class);
            if (null == muleTcc) continue;
            try {
                return MuleTccProxyInvocationHandler.generateProxyObject(methodProxyService, handler, bean, Class.forName(beanName));
            } catch (ClassNotFoundException e) {
                throw new BeanCurrentlyInCreationException(beanName, StrKit.format("【{}】找不到指定的class类型", beanName));
            }
        }
        return bean;
    }
}
