package net.lulihu.mule.tccTransaction;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.mule.tccTransaction.banner.MuleTccBanner;
import net.lulihu.mule.tccTransaction.service.MuleTccBootService;
import net.lulihu.mule.tccTransaction.service.factory.TransactionFactoryComponent;

/**
 * mule tcc事务启动应用程序
 */
@Slf4j
public class MuleTccTransactionBootApplication {

    private final MuleTccBootService muleTccBootService;

    private final MuleTccConfig muleTccConfig;

    public MuleTccTransactionBootApplication(MuleTccBootService muleTccBootService, MuleTccConfig muleTccConfig) {
        this.muleTccBootService = muleTccBootService;
        this.muleTccConfig = muleTccConfig;

        // 添加应用程序关闭钩子到jvm中，在jvm关闭时调用
        // 注意linux 杀死进程的指令 kill -9 将导致该回调不执行。请使用kill -15由操作系统提醒jvm自行关闭
        Runtime.getRuntime().addShutdownHook(new Thread(MuleTccShutdownManage.getInstance()::startShutdown, "Mule Tcc 关闭程序"));
    }

    /**
     * 开始启动应用程序
     */
    public void start() {
        // 加载banner图
        MuleTccBanner.banner();

        LogKit.info(log, "Mule Tcc 分布式事务协调器 开始启动...");
        try {
            // 初始化程序
            muleTccBootService.initialization(muleTccConfig);
            // 启动组件工厂，加载组件
            TransactionFactoryComponent.startFactory();

            LogKit.info(log, "Mule Tcc 分布式事务协调器 启动成功...");
        } catch (Throwable e) {
            LogKit.error(log, "初始化Mule Tcc失败", e);
            System.exit(-1);
        }
    }

}
