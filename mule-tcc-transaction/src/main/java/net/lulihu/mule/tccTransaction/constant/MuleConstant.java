package net.lulihu.mule.tccTransaction.constant;

/**
 * 常量方法
 */
public final class MuleConstant {

    /**
     * 跟在请求头部中的事务上下文对应的key
     */
    public final static String TRANSACTION_CONTEXT = "MULE_TCC_TRANSACTION_CONTEXT";

    /**
     * 事务补偿记录表名添加后缀区分
     */
    public final static String COMPENSATIONS_SUFFIX = "_compensations";

}
