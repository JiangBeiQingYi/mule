package net.lulihu.mule.tccTransaction.service.impl;

import net.lulihu.mule.tccTransaction.exception.NotFindSuitableObjectException;
import net.lulihu.mule.tccTransaction.model.TransactionContext;
import net.lulihu.mule.tccTransaction.service.TransactionExecutorEventService;
import net.lulihu.mule.tccTransaction.service.factory.TransactionFactoryComponent;
import net.lulihu.mule.tccTransaction.service.factory.TransactionComponentFactoryService;
import net.lulihu.mule.tccTransaction.service.TransactionHandlerService;

import java.util.Collection;

/**
 * 默认事务工厂组件管理实现
 */
public class DefaultTransactionComponentFactoryManage implements TransactionComponentFactoryService {

    @Override
    public TransactionHandlerService electionTransactionHandler(TransactionContext context) throws NotFindSuitableObjectException {
        Collection<TransactionHandlerService> handlerServiceSet = TransactionFactoryComponent.getWorkshopComponentSetByClazz(TransactionHandlerService.class);
        // 循环寻找合适的处理器对象
        for (TransactionHandlerService handlerService : handlerServiceSet) {
            boolean support = handlerService.support(context);
            if (support) return handlerService;
        }
        // 没有合适的抛出异常
        throw new NotFindSuitableObjectException("事务上下文:{}, 无法找到合适的处理器，执行中断...", context);
    }


    @Override
    public TransactionExecutorEventService electionTransactionExecutor(TransactionHandlerService handlerService) throws NotFindSuitableObjectException {
        Collection<TransactionExecutorEventService> executorServiceSet =
                TransactionFactoryComponent.getWorkshopComponentSetByClazz(TransactionExecutorEventService.class);
        // 循环寻找合适的执行者对象
        for (TransactionExecutorEventService executorService : executorServiceSet) {
            boolean support = executorService.support(handlerService);
            if (support) return executorService;
        }
        // 没有合适的抛出异常
        throw new NotFindSuitableObjectException("事务处理器【{}】，未找到合适的执行者，启动失败...", handlerService.getClass());
    }


}
