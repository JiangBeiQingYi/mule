package net.lulihu.mule.tccTransaction.kit;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.BeanKit;
import net.lulihu.ObjectKit.ClassKit;
import net.lulihu.ObjectKit.ReflectKit;
import net.lulihu.mule.tccTransaction.exception.RepositoryMapperException;
import net.lulihu.mule.tccTransaction.serializer.ObjectSerializer;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;

/**
 * 查询结果集封装
 */
@Slf4j
public class QueryResultSetKit {

    /**
     * 查询结果赋值
     *
     * @param serializer  序列化对象
     * @param resultSet   查询结果集
     * @param resultClass 返回结果的类型
     * @return 查询结果封装至对应的类中
     */
    public static Object putResultSet(ObjectSerializer serializer, ResultSet resultSet, Class<?> resultClass)
            throws Exception {
        int resultType; // 1.基本数据类型 2.封装bean
        if (BeanKit.isPrimitive(resultClass)) resultType = 1;
        else if (BeanKit.isBean(resultClass)) resultType = 2;
        else throw new RepositoryMapperException("错误的返回值类型【{}】", resultClass);

        return putResultSetToArray(resultType, serializer, resultSet, resultClass);
    }

    /**
     * 给返回值为集合的对象赋值
     *
     * @param resultType  返回值类型 1.基本数据类型 2.封装bean
     * @param serializer  序列化对象
     * @param resultSet   查询结果集
     * @param resultClass 结果集封装
     * @return 查询结果封装至对应的类中
     */
    private static Object putResultSetToArray(int resultType, ObjectSerializer serializer,
                                              ResultSet resultSet, Class<?> resultClass) throws Exception {
        List<Object> result = new ArrayList<>();
        ResultSetMetaData data = resultSet.getMetaData();

        while (resultSet.next()) {
            Object value = (resultType == 1 ? resultSet.getObject(1) :
                    putResultSet(serializer, resultClass, resultSet, data));
            result.add(value);
        }
        return result;
    }

    /**
     * 给返回值为 Bean 的赋值
     *
     * @param serializer  序列化对象
     * @param resultClass 结果封装类型
     * @param resultSet   查询结果集
     * @param data        查询结果集元数据
     * @return 查询结果封装至对应的类中
     */
    private static Object putResultSet(ObjectSerializer serializer, Class<?> resultClass,
                                       ResultSet resultSet, ResultSetMetaData data) throws Exception {
        // 列长度
        int count = data.getColumnCount();
        Object result = ClassKit.newInstance(resultClass);

        // 获取bean属性名称
        // key->原对象属性名称的驼峰或者下划线模式  value 原对象属性名称
        Map<String, String> propertyName = BeanKit.getBeanPropertyName(resultClass);

        for (int i = 1; i <= count; i++) {
            // 列名称
            String columnName = data.getColumnLabel(i);
            // 通过列名称获取当前行的值
            Object value = resultSet.getObject(columnName);
            // 如果为空值，跳过
            if (value == null) continue;

            // 名称校验
            if (propertyName.containsKey(columnName)) {
                String fieldName = propertyName.get(columnName);
                // 反序列化
                if (value instanceof byte[]) {
                    Field targetField = ReflectKit.getTargetField(resultClass, fieldName);
                    Class<?> type = targetField.getType();
                    if (!value.getClass().equals(type)) {
                        if (type.equals(List.class)) type = ArrayList.class;
                        else if (type.equals(Map.class)) type = HashMap.class;
                        else if (type.equals(Set.class)) type = HashSet.class;

                        value = serializer.deSerialize((byte[]) value, type);
                    }
                }
                ReflectKit.setFieldValue(result, fieldName, value);
            }
        }
        return result;
    }

}
