package net.lulihu.mule.tccTransaction.service.factory;


import net.lulihu.mule.tccTransaction.service.ComponentService;
import net.lulihu.mule.tccTransaction.service.TransactionSupportService;

/**
 * 事务工厂顶层超类
 */
public interface TransactionFactoryService<T> extends ComponentService, TransactionSupportService<T> {

    /**
     * 事务工厂内组件全部初始化结束后执行目标方法
     *
     * @throws Exception 方法如果抛出异常则程序启动失败
     */
    default void afterInitialization() throws Exception {
    }

}
