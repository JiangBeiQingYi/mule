package net.lulihu.mule.tccTransaction.kit;

import net.lulihu.mule.tccTransaction.constant.MuleConstant;
import net.lulihu.mule.tccTransaction.enums.DbTypeEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;

import java.util.Arrays;

/**
 * 建表数据库语句根据
 */
public class TableSqlKit {

    /**
     * 获取建表语句
     *
     * @param driverClassName 驱动程序全类名
     * @param tableName       数据库表名称
     * @return 建表语句
     */
    public static String[] getCreateTableSql(String driverClassName, String tableName) {
        return getCreateTableSql(DbTypeEnum.getDbTypeByDriverClassName(driverClassName), tableName);
    }

    public static String[] getCreateTableSql(DbTypeEnum dbTypeEnum, String tableName) {
        if (dbTypeEnum.equals(DbTypeEnum.MYSQL)) {
            return new String[]{transactionLogMysql(tableName), transactionCompensationsMysql(tableName)};
        }
        throw new MuleTccException("不支持的DB类型，目前只支持{}", Arrays.asList(DbTypeEnum.values()));
    }

    /**
     * 构建mysql事务记录建表语句
     *
     * @param tableName 数据库表名称
     * @return 建表语句
     */
    private static String transactionLogMysql(String tableName) {
        return "CREATE TABLE IF NOT EXISTS `" + tableName + "` " +
                "(" +
                "  `trans_id` varchar(64) NOT NULL COMMENT '事务id'," +
                "  `status` tinyint NOT NULL COMMENT '事务执行状态'," +
                "  `role` tinyint NOT NULL COMMENT '事务执行角色'," +
                "  `target_class` varchar(256) COMMENT '执行目标全类名'," +
                "  `target_method` varchar(128) COMMENT '执行目标方法名称'," +
                "  `participants` longblob COMMENT '事务参与者'," +
                "  `version` tinyint NOT NULL COMMENT '事务补偿时使用的乐观锁版本号'," +
                "  `create_time` varchar(64) NOT NULL COMMENT '事务记录创建时间 yyyy-MM-dd HH:mm:ss.SSS'," +
                "  `last_time` varchar(64) NOT NULL COMMENT '事务记录最后修改时间 yyyy-MM-dd HH:mm:ss.SSS'," +
                "  PRIMARY KEY (`trans_id`)" +
                ") COMMENT = '服务【" + tableName + "】的事务记录表'";
    }

    /**
     * 构建mysql事务补偿记录建表语句
     *
     * @param tableName 数据库表名称
     * @return 建表语句
     */
    private static String transactionCompensationsMysql(String tableName) {
        return "CREATE TABLE IF NOT EXISTS `" + tableName + MuleConstant.COMPENSATIONS_SUFFIX + "`" +
                "(" +
                "  `trans_id` varchar(64)  NOT NULL COMMENT '事务id'," +
                "  `status` tinyint(4) NOT NULL COMMENT '事务补偿状态'," +
                "  `create_time` varchar(64) NOT NULL COMMENT '事务补偿记录创建时间 yyyy-MM-dd HH:mm:ss.SSS'," +
                "  PRIMARY KEY (`trans_id`)" +
                ") COMMENT = '服务【" + tableName + "】的事务补偿记录表'";
    }

}
