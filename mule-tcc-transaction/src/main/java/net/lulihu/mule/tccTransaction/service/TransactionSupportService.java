package net.lulihu.mule.tccTransaction.service;


/**
 * 是否支持当前的处理
 * <p>
 * 用来选择事务处理程序
 */
public interface TransactionSupportService<T> {

    /**
     * 该处理器是否支持处理该事务
     *
     * @param obj 泛型由实现者决定
     * @return true 为支持 反之不支持
     */
    boolean support(T obj);

    /**
     * 根据该方法返回的值进行排序，值越小越先执行判断方法
     *
     * @return 处理器执行顺序 默认为 0
     */
    default int order() {
        return 0;
    }
}
