package net.lulihu.mule.tccTransaction.exception;

import net.lulihu.ObjectKit.StrKit;
import net.lulihu.exception.ExceptionEnum;

/**
 * 工厂组件Class 强制转换异常
 */
public class FactoryCompontClassCastException extends RuntimeException {

    public FactoryCompontClassCastException() {
        super();
    }

    public FactoryCompontClassCastException(ExceptionEnum exceptionEnum) {
        this(exceptionEnum.getMessage());
    }

    public FactoryCompontClassCastException(String message) {
        super(message);
    }

    public FactoryCompontClassCastException(String message, Object... values) {
        super(StrKit.format(message, values));
    }

    public FactoryCompontClassCastException(Throwable cause) {
        super(cause);
    }

    public FactoryCompontClassCastException(String message, Throwable cause) {
        super(message, cause);
    }

}
