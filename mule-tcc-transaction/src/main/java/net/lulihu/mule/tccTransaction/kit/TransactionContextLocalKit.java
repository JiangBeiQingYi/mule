package net.lulihu.mule.tccTransaction.kit;

import net.lulihu.ObjectKit.ConcurrentThreadLocalKit;
import net.lulihu.mule.tccTransaction.enums.ThreadLocalEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;
import net.lulihu.mule.tccTransaction.model.TransactionContext;

/**
 * 本地事务上下文副本
 */
public class TransactionContextLocalKit {

    private static final ConcurrentThreadLocalKit threadLocal = ConcurrentThreadLocalKit.getInstance();

    private static final String key = ThreadLocalEnum.TRANSACTION_CONTEXT.getDescription();

    /**
     * 构造函数私有化
     */
    private TransactionContextLocalKit() {
        if (threadLocal.containsKey(key))
            throw new MuleTccException("当前线程副本中已经存在指定名称【{}】的数据副本", key);

    }

    /**
     * 设置本地事务上下文
     *
     * @param transactionContext 事务上下文
     */
    public static void set(TransactionContext transactionContext) {
        threadLocal.set(key, transactionContext);
    }

    /**
     * 获取本地事务上下文
     */
    public static TransactionContext get() {
        return (TransactionContext) threadLocal.get(key);
    }

    /**
     * 删除当前线程的事务上下文
     */
    public static void clear() {
        threadLocal.clear(key);
    }

}
