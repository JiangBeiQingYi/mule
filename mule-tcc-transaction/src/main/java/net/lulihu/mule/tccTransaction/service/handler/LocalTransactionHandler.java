package net.lulihu.mule.tccTransaction.service.handler;

import net.lulihu.mule.tccTransaction.enums.MuleRoleEnum;
import net.lulihu.mule.tccTransaction.kit.TransactionKit;
import net.lulihu.mule.tccTransaction.kit.TransactionLogLocalKit;
import net.lulihu.mule.tccTransaction.model.MuleParticipant;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.TransactionContext;

import java.lang.reflect.Method;

/**
 * 本地参与者处理器
 */
public class LocalTransactionHandler extends AbstractTransactionHandlerService {

    @Override
    public boolean support(TransactionContext context) {
        if (context == null) return false;
        int role = context.getRole();
        return role == MuleRoleEnum.START_LOCAL.getCode() || role == MuleRoleEnum.RPC_LOCAL.getCode();
    }

    @Override
    public boolean beforeHandler(Class<?> beanClass, Method method, Object[] args) {
        MuleTransaction muleTransaction = TransactionLogLocalKit.get();
        // 添加参与者
        TransactionKit.addMuleParticipant(muleTransaction, beanClass, method, args);
        // 修改参与者记录
        transactionExecutorService.updateTransactionParticipant(muleTransaction);
        return true;
    }


    @Override
    public Object exceptionHandler(Throwable throwable) throws Throwable {
        MuleTransaction muleTransaction = TransactionLogLocalKit.get();
        MuleParticipant muleParticipant = muleTransaction.lastOne();
        // 参与者方法执行发生例外，判断是否参与回滚方法
        if (muleParticipant.getParticipateCompensation()) {
            muleParticipant.setParticipateCompensation(false);
            transactionExecutorService.updateTransactionParticipant(muleTransaction);
        }
        throw throwable;
    }

    @Override
    public String componentName() {
        return "事务本地参与者处理器";
    }
}
