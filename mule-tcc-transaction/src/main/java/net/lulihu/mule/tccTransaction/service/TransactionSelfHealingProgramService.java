package net.lulihu.mule.tccTransaction.service;

import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.service.factory.TransactionFactoryService;

/**
 * 事务自我恢复程序
 *
 * @param <T>
 */
public interface TransactionSelfHealingProgramService<T> extends TransactionFactoryService<T> {

    @Override
    default boolean support(T obj) {
        return true;
    }

    /**
     * 初始化应用程序
     *
     * @param config 配置
     */
    void initialization(TransactionCoordinatorService transactionCoordinatorService, MuleTccConfig config) throws Exception;

    @Override
    default String componentName() {
        return "事务自我恢复程序";
    }


}
