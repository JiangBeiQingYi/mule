package net.lulihu.mule.tccTransaction.service;

import net.lulihu.mule.tccTransaction.model.TransactionContext;
import net.lulihu.mule.tccTransaction.service.factory.TransactionFactoryService;

import java.lang.reflect.Method;

/**
 * 事务处理器 服务
 */
public interface TransactionHandlerService extends TransactionFactoryService<TransactionContext> {

    /**
     * 事务方法执行前执行的处理器方法
     *
     * @param beanClass 代理方法的目标对象类型
     * @param method    代理事务方法
     * @param args      代理事务方法参数
     * @return true继续执行目标方法反之不执行目标方法，返回一个默认结果
     */
    default boolean beforeHandler(Class<?> beanClass, Method method, Object[] args) {
        return true;
    }

    /**
     * 事务方法执行后的处理器执行方法
     *
     * @param result 事务方法处理结果
     * @return 返回新的处理结果
     */
    default Object resultHandler(Object result) {
        return result;
    }

    /**
     * 事务方法执行过程中发送例外情况对应的处理器执行的方法
     *
     * @param throwable 异常
     * @return 返回新的处理结果
     */
    default Object exceptionHandler(Throwable throwable) throws Throwable {
        throw throwable;
    }

    /**
     * 事务方法执行后的处理器执行方法
     */
    default void afterHandler() {
    }


}
