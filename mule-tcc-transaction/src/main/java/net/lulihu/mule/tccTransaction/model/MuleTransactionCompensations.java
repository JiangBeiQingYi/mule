
package net.lulihu.mule.tccTransaction.model;

import lombok.Data;
import lombok.ToString;
import net.lulihu.mule.tccTransaction.annotation.DbField;
import net.lulihu.mule.tccTransaction.enums.MuleActionEnum;

import java.io.Serializable;

/**
 * 记录当前服务事务参数
 */
@Data
@ToString
public class MuleTransactionCompensations implements Serializable {

    /**
     * 事务id
     */
    @DbField("trans_id")
    private String transId;

    /**
     * 事务状态
     * {@link MuleActionEnum}
     */
    @DbField("status")
    private int status;

    /**
     * 创建时间 yyyy-MM-dd HH:mm:ss.SSS
     */
    @DbField("create_time")
    private String createTime;
}
