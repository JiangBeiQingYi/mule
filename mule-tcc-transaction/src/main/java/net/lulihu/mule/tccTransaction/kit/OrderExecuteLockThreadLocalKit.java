package net.lulihu.mule.tccTransaction.kit;


import net.lulihu.ObjectKit.ConcurrentThreadLocalKit;
import net.lulihu.lock.OrderExecuteLockKit;
import net.lulihu.mule.tccTransaction.enums.ThreadLocalEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;

/**
 * 按序执行锁线程副本
 */
public class OrderExecuteLockThreadLocalKit {

    private static final ConcurrentThreadLocalKit threadLocal = ConcurrentThreadLocalKit.getInstance();

    private static final String key = ThreadLocalEnum.ORDER_EXECUTE_LOCK.getDescription();

    /**
     * 构造函数私有化
     */
    private OrderExecuteLockThreadLocalKit() {
        if (threadLocal.containsKey(key))
            throw new MuleTccException("当前线程副本中已经存在指定名称【{}】的数据副本", key);

    }

    /**
     * 设置按序执行锁工具线程副本
     */
    public static void set() {
        if (!threadLocal.containsKey(key)) {
            threadLocal.set(key, new OrderExecuteLockKit());
        }
    }

    /**
     * 获取按序执行锁工具线程副本
     */
    public static OrderExecuteLockKit get() {
        return (OrderExecuteLockKit) threadLocal.get(key);
    }

    /**
     * 删除当前线程的按序执行锁工具线程副本
     */
    public static void clear() {
        threadLocal.clear(key);
    }
}
