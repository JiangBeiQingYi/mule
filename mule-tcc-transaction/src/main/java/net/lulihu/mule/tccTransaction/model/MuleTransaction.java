
package net.lulihu.mule.tccTransaction.model;

import lombok.Data;
import lombok.ToString;
import net.lulihu.dateTime.DateTimeKit;
import net.lulihu.mule.tccTransaction.annotation.DbField;
import net.lulihu.mule.tccTransaction.enums.MuleActionEnum;
import net.lulihu.mule.tccTransaction.enums.MuleRoleEnum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 记录当前服务事务参数
 */
@Data
@ToString
public class MuleTransaction implements Serializable {

    /**
     * 事务id
     */
    @DbField("trans_id")
    private String transId;

    /**
     * 事务状态
     * {@link MuleActionEnum}
     */
    @DbField("status")
    private int status;

    /**
     * 事务角色
     * {@link  MuleRoleEnum}
     */
    @DbField("role")
    private int role;

    /**
     * 调用接口对象全类名
     */
    @DbField("target_class")
    private String targetClass;

    /**
     * 调用接口方法名称。
     */
    @DbField("target_method")
    private String targetMethod;

    /**
     * 参与事务协调的参与者对象信息
     */
    @DbField(value = "participants", serializer = true)
    private List<MuleParticipant> participants;

    /**
     * 数据库乐观锁版本号，在启动自我恢复程序时使用，避免并发修改带来的问题
     */
    @DbField("version")
    private int version = 1;

    /**
     * 创建时间 yyyy-MM-dd HH:mm:ss.SSS
     */
    @DbField("create_time")
    private String createTime;

    /**
     * 最后修改时间 yyyy-MM-dd HH:mm:ss.SSS
     */
    @DbField("last_time")
    private String lastTime;

    public MuleTransaction() {
    }

    public MuleTransaction(String transId) {
        this.transId = transId;
        this.createTime = DateTimeKit.date().toString(DateTimeKit.NORM_DATETIME_MS_PATTERN);
        this.lastTime = this.createTime;
        this.participants = new ArrayList<>();
    }

    /**
     * 注册参与者
     *
     * @param muleParticipant {@link  MuleParticipant}
     */
    public void registerParticipant(MuleParticipant muleParticipant) {
        this.participants.add(muleParticipant);
    }

    /**
     * 获取最后一个参与者
     *
     * @return 返回参数者
     */
    public MuleParticipant lastOne() {
        return this.participants.get(this.participants.size() - 1);
    }

}
