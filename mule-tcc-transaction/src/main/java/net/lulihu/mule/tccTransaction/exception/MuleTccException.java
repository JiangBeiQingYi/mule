package net.lulihu.mule.tccTransaction.exception;

import net.lulihu.ObjectKit.StrKit;
import net.lulihu.exception.ExceptionEnum;

/**
 * 异常封装
 */
public class MuleTccException extends RuntimeException {

    public MuleTccException() {
        super();
    }

    public MuleTccException(ExceptionEnum exceptionEnum) {
        this(exceptionEnum.getMessage());
    }

    public MuleTccException(String message) {
        super(message);
    }


    public MuleTccException(String message, Object... values) {
        super(StrKit.format(message, values));
    }

    public MuleTccException(Throwable cause) {
        super(cause);
    }

    public MuleTccException(String message, Throwable cause) {
        super(message, cause);
    }
}
