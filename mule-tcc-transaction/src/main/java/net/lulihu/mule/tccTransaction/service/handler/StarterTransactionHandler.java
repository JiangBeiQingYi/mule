package net.lulihu.mule.tccTransaction.service.handler;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.ObjectKit.ThreadLocalKit;
import net.lulihu.mule.tccTransaction.enums.MuleRoleEnum;
import net.lulihu.mule.tccTransaction.kit.*;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.TransactionContext;

import java.lang.reflect.Method;

/**
 * 事务发起者处理器
 */
@Slf4j
public class StarterTransactionHandler extends AbstractTransactionHandlerService {

    /**
     * 事务上下文为空则执行，表示为目标方法为事务发起者
     *
     * @param transactionContext 事务上下文
     */
    @Override
    public boolean support(TransactionContext transactionContext) {
        return MuleRoleEnum.START.getCode() == transactionContext.getRole();
    }

    /*
     * 注:因为事务记录对象是引用传递，如果指定的try方法执行速度较快，在生成事务记录插入模板前，
     *  就已经执行结束，那将导致插入的事务数据对应的状态字段为结束的状态字段，这并不影响。
     */
    @Override
    public boolean beforeHandler(Class<?> beanClass, Method method, Object[] args) {
        LogKit.debug(log, "【{}】代理方法【{}】事务启动...", beanClass, method.getName());
        TransactionContext context = TransactionContextLocalKit.get();

        // 构建当前服务的事务记录
        MuleTransaction muleTransaction = TransactionKit.buildMuleTransaction(
                context.getTransId(), MuleRoleEnum.START, beanClass, method, args);
        TransactionLogLocalKit.set(muleTransaction); // 保存在本地副本中
        transactionExecutorService.saveTransaction(muleTransaction); // 保存事务记录
        return true;
    }

    @Override
    public Object resultHandler(Object result) {
        if (ServletRequestLocalKit.notRequest()) {
            StarterTransactionHandlerKit.exeConfirm(transactionExecutorService);
        }
        return result;
    }

    @Override
    public Object exceptionHandler(Throwable throwable) throws Throwable {
        if (ServletRequestLocalKit.notRequest()) {
            StarterTransactionHandlerKit.exeCancel(transactionExecutorService);
        }
        throw throwable;
    }

    @Override
    public void afterHandler() {
        // 如果事务处理单位不是请求
        if (ServletRequestLocalKit.notRequest()) {
            ThreadLocalKit.clearAll();
        } else {
            // 反之将发起者的事务执行者放置在当前的缓存中，直到当前请求结束，由拦截器进行事务提交
            StarterTransactionExecutorEventLocalKit.set(transactionExecutorService);
        }
    }

    @Override
    public String componentName() {
        return "事务发起者处理器";
    }
}
