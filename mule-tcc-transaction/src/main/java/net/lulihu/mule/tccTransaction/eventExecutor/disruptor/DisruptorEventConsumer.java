package net.lulihu.mule.tccTransaction.eventExecutor.disruptor;

import com.lmax.disruptor.WorkHandler;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.MethodKit;
import net.lulihu.ObjectKit.ThreadLocalKit;
import net.lulihu.disruptorKit.Event;
import net.lulihu.mule.tccTransaction.service.TransactionCoordinatorService;


/**
 * 消费者线程
 */
@Slf4j
public class DisruptorEventConsumer implements WorkHandler<Event<DisruptorTransactionEvent>> {

    private final TransactionCoordinatorService transactionCoordinatorService;

    public DisruptorEventConsumer(TransactionCoordinatorService transactionCoordinatorService) {
        this.transactionCoordinatorService = transactionCoordinatorService;
    }

    @Override
    public void onEvent(Event<DisruptorTransactionEvent> event) {
        try {
            DisruptorTransactionEvent element = event.getElement();
            EventTypeEnum eventType = element.getEventType();
            Object[] args = element.getArgs();

            // 执行目标方法
            MethodKit.invokeExactMethodNotException(transactionCoordinatorService, eventType.getMethodName(), args);
        } finally {
            // 方便gc
            event.clear();
            // 清除当前线程副本信息
            ThreadLocalKit.clearAll();
        }
    }


}
