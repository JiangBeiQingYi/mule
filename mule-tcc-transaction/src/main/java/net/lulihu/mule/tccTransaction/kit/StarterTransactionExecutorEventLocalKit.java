package net.lulihu.mule.tccTransaction.kit;

import net.lulihu.ObjectKit.ConcurrentThreadLocalKit;
import net.lulihu.mule.tccTransaction.enums.ThreadLocalEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;
import net.lulihu.mule.tccTransaction.service.TransactionExecutorEventService;

/**
 * 当前发起者事务处理器对象使用的执行者对象副本工具
 * <p>
 * 当事务单位为请求时，起传递执行者对象作用
 */
public class StarterTransactionExecutorEventLocalKit {

    private static final ConcurrentThreadLocalKit threadLocal = ConcurrentThreadLocalKit.getInstance();

    private static final String key = ThreadLocalEnum.STARTER_TRANSACTION_EXECUTOR.getDescription();

    /**
     * 构造函数私有化
     */
    private StarterTransactionExecutorEventLocalKit() {
        if (threadLocal.containsKey(key))
            throw new MuleTccException("当前线程副本中已经存在指定名称【{}】的数据副本", key);

    }

    /**
     * 设置当前发起者事务处理器对象使用的执行者对象
     *
     * @param transactionExecutorEventService 执行者对象
     */
    public static void set(TransactionExecutorEventService transactionExecutorEventService) {
        threadLocal.set(key, transactionExecutorEventService);
    }

    /**
     * 获取当前发起者事务处理器对象使用的执行者对象
     */
    public static TransactionExecutorEventService get() {
        return (TransactionExecutorEventService) threadLocal.get(key);
    }

    /**
     * 删除当前发起者事务处理器对象使用的执行者对象
     */
    public static void clear() {
        threadLocal.clear(key);
    }

}
