
package net.lulihu.mule.tccTransaction.serializer;

import net.lulihu.mule.tccTransaction.enums.SerializeEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Java 原生序列化
 */
@SuppressWarnings("unchecked")
public class JavaSerializer implements ObjectSerializer {

    @Override
    public byte[] serialize(final Object obj) throws MuleTccException {
        try (ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
             ObjectOutput objectOutput = new ObjectOutputStream(arrayOutputStream)) {
            objectOutput.writeObject(obj);
            objectOutput.flush();
            return arrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new MuleTccException("java序列化时发生例外 ", e);
        }
    }

    @Override
    public <T> T deSerialize(final byte[] param, final Class<T> clazz) throws MuleTccException {
        try (ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(param);
             ObjectInput input = new ObjectInputStream(arrayInputStream)) {
            return (T) input.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new MuleTccException("java反序列化时发生例外", e);
        }
    }

    @Override
    public SerializeEnum getScheme() {
        return SerializeEnum.JDK;
    }
}
