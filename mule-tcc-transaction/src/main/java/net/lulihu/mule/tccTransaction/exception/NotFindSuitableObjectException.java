package net.lulihu.mule.tccTransaction.exception;


import net.lulihu.ObjectKit.StrKit;
import net.lulihu.exception.ExceptionEnum;

/**
 * 找不到合适的对象例外
 */
public class NotFindSuitableObjectException extends RuntimeException {

    public NotFindSuitableObjectException() {
        super();
    }

    public NotFindSuitableObjectException(ExceptionEnum exceptionEnum) {
        this(exceptionEnum.getMessage());
    }

    public NotFindSuitableObjectException(String message) {
        super(message);
    }

    public NotFindSuitableObjectException(String message, Object... values) {
        super(StrKit.format(message, values));
    }

    public NotFindSuitableObjectException(Throwable cause) {
        super(cause);
    }

    public NotFindSuitableObjectException(String message, Throwable cause) {
        super(message, cause);
    }
}
