package net.lulihu.mule.tccTransaction.service;


/**
 * mule Tcc 关闭应用程序
 */
public interface MuleTccShutdownService extends ComponentService {

    /**
     * 关闭应用程序
     */
    void shutdown() throws Exception;

    /**
     * 关闭应用程序时，内部组件的关闭顺序
     * <p>
     * 如果为负数则表示不需要排序，反之 从0开始依次递增为关闭顺序
     *
     * @return 组件关闭顺序
     */
    default int order() {
        return -1;
    }


}
