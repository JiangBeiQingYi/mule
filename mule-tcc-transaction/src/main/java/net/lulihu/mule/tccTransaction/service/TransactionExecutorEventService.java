package net.lulihu.mule.tccTransaction.service;

import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.TransactionContext;

/**
 * tcc事务事件服务
 */
public interface TransactionExecutorEventService  extends TransactionExecutorService<TransactionHandlerService> {

    /**
     * 保存事务记录
     *
     * @param muleTransaction 事务记录
     */
    void saveTransaction(MuleTransaction muleTransaction);

    /**
     * 修改事件参与者
     *
     * @param muleTransaction 事件记录
     */
    void updateTransactionParticipant(MuleTransaction muleTransaction);

    /**
     * 执行取消方法
     *
     * @param muleTransaction    事务记录
     * @param transactionContext 事务上下文
     */
    void cancel(MuleTransaction muleTransaction, TransactionContext transactionContext);

    /**
     * 执行确认方法
     *
     * @param muleTransaction    事务记录
     * @param transactionContext 事务上下文
     */
    void confirm(MuleTransaction muleTransaction, TransactionContext transactionContext);

    /**
     * rpc 服务执行确认方法
     *
     * @param context 事务上下文
     */
    void rpcConfirm(TransactionContext context);

    /**
     * rpc 服务执行取消方法
     *
     * @param context 事务上下文
     */
    void rpcCancel(TransactionContext context);

    /**
     * 删除事务记录
     *
     * @param muleTransaction 事务记录
     */
    void delete(MuleTransaction muleTransaction);

    /**
     * 删除事务记录
     *
     * @param context 事务上下文
     */
    void deleteByContext(TransactionContext context);

    /**
     * 修改事务状态
     *
     * @param muleTransaction 事务记录
     */
    void updateStatus(MuleTransaction muleTransaction);
}
