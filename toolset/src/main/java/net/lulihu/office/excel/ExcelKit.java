package net.lulihu.office.excel;

import net.lulihu.ObjectKit.FileKit;
import net.lulihu.ObjectKit.MapKit;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import net.lulihu.Assert;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * excel导入导出常用工具类
 */
public class ExcelKit {

    /**
     * 读取excel文件数据
     *
     * @param filePath       文件路径
     * @param continueRowMap 多单元根据那些列为空来忽略行数据（可为空，如 mapContinueRow.put(1,new Integer[]{1, 3}); 第一个表格从1、3列为空就忽略）
     * @throws IOException
     */
    public static LinkedHashMap<String, ExcelReadHandle> excelReadInFile(String filePath,
                                                                         HashMap continueRowMap) throws IOException {
        Assert.isTrue(FileKit.isEmpty(filePath), "文件不存在");

        LinkedHashMap<String, List<List<String>>> excelData = null;
        try (InputStream inputStream = new FileInputStream(filePath)) {
            excelData = ExcelAnalysis.importForExcelData(new XSSFWorkbook(inputStream), continueRowMap);
        } catch (FileNotFoundException ignored) {
            // 已经做过判断 忽略
        }
        LinkedHashMap<String, ExcelReadHandle> result = new LinkedHashMap<>();
        if (MapKit.isEmpty(excelData)) return result;

        for (String key : excelData.keySet()) {
            result.put(key, new ExcelReadHandle(excelData.get(key)));
        }
        return result;
    }


    /**
     * 写出excel到文件
     *
     * @param excel    导出信息
     * @param filePath 文件路径
     */
    public static void excelWriteToFile(Excel excel, String filePath) {
        Assert.notNull(excel, "导出数据对象不可以为空");
        Assert.notNull(filePath, "文件路径不可以为空");

        ExcelAnalysis excelUtils = new ExcelAnalysis();
        // 必填项--导出数据.
        excelUtils.setDataLists(excel.getData());
        // sheet名称
        excelUtils.setSheetName(excel.getSheetName());
        // 每个表格的大标题（可为空）
        excelUtils.setLabelName(excel.labelFirstMerge());
        //  输出本地路径
        excelUtils.setFilePath(filePath);
        // 执行导出
        excelUtils.exportForExcelsOptimize();
    }

    /**
     * 导出数据流到响应体中
     *
     * @param response 响应体
     * @param excel    导出信息
     * @param fileName 文件名称
     */
    public static void excelWriteToResponse(HttpServletResponse response, Excel excel, String fileName) {
        Assert.notNull(response, "响应体不可以为空");
        Assert.notNull(excel, "导出数据对象不可以为空");
        Assert.notNull(fileName, "文件名称不可以为空");

        ExcelAnalysis excelUtils = new ExcelAnalysis();
        // 必填项--导出数据.
        excelUtils.setDataLists(excel.getData());
        // sheet名称
        excelUtils.setSheetName(excel.getSheetName());
        // 每个表格的大标题（可为空）
        excelUtils.setLabelName(excel.labelFirstMerge());
        // 文件名称(可为空，默认是：sheet 第一个名称)
        excelUtils.setFileName(fileName);
        //  输出本地路径
        excelUtils.setResponse(response);
        // 执行导出
        excelUtils.exportForExcelsOptimize();
    }
}
