package net.lulihu.office.excel;

import net.lulihu.Assert;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * excel 中的每个sheet页的数据处理对象
 */
public class ExcelReadHandle {

    /**
     * 列名称，列索引
     */
    private Map<String, Integer> nameData;

    /**
     * 表格数据 除去第一行作为标题
     */
    private List<List<String>> data;

    /**
     * 行长度
     */
    private int size;

    public ExcelReadHandle(List<List<String>> data) {
        Assert.notNull(data, "数据不可以为空");

        // 读取第一行的名称
        AtomicInteger i = new AtomicInteger(-1);
        this.nameData = data.remove(0).stream().collect(
                Collectors.toMap(
                        Function.identity(),
                        v -> i.incrementAndGet()));
        this.data = data;
        this.size = data.size();
    }

    /**
     * 循环所有的行数
     *
     * @param action 表达式 （this,行索引）
     */
    public void forEachRow(BiConsumer<ExcelReadHandle, ? super Integer> action) {
        Objects.requireNonNull(action);
        for (int i = 0; i < size; i++) {
            action.accept(this, i);
        }
    }

    /**
     * 循环所有的行数据
     *
     * @param action 表达式 （this,行数据）
     */
    public void forEachRowData(BiConsumer<ExcelReadHandle, ? super List<String>> action) {
        Objects.requireNonNull(action);
        for (int i = 0; i < size; i++) {
            action.accept(this, data.get(i));
        }
    }

    /**
     * 获取指定行数对应列名称的值
     *
     * @param row 行数
     * @param key 列名
     */
    public Object getValue(int row, String key) {
        if (size < row)
            return null;
        Integer col = nameData.get(key);
        if (col == null)
            return null;
        return data.get(row).get(col);
    }

    /**
     * 获取指定列名称对应列名称的值
     *
     * @param rowData 行数据
     * @param key     列名
     */
    public String getValue(List<String> rowData, String key) {
        Integer col = nameData.get(key);
        if (col == null)
            return null;
        return rowData.get(col);
    }
}
