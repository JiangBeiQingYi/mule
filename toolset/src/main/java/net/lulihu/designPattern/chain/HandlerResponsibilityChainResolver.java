package net.lulihu.designPattern.chain;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 责任链处理解析器
 *
 * @param <T>
 */
public interface HandlerResponsibilityChainResolver<T> {

    Logger log = LoggerFactory.getLogger(HandlerResponsibilityChainResolver.class);

    /**
     * 当前对象是否支持给定的参数处理
     *
     * @param data 参数
     * @return true则处理 反之不处理
     */
    default boolean support(T data) {
        return true;
    }

    /**
     * 此方法对给定的参数进行程序处理
     *
     * @param data 参数
     */
    void resolve(T data) throws ChainEventException;

    /**
     * 此方法对给定的参数进行程序判断，是否还要交由下一个人处理
     *
     * @param data 参数
     * @return true则后续继续处理 反之链条结束
     */
    default boolean proceed(T data) {
        return true;
    }

    /**
     * 程序处理之后执行
     *
     * @param data 参数
     * @return 返回处理结果该结果将被下一个处理程序使用
     */
    default T after(T data) {
        return data;
    }


    /**
     * 处理程序发生可控异常时执行
     *
     * @param data 参数
     * @return true则后续继续处理 反之链条结束
     */
    default boolean error(T data, ChainEventException e) {
        log.error("处理程序发生例外...", e);
        return false;
    }

    /**
     * 程序执行结束进行最终处理，可以影响最终的返回值
     *
     * @param data 参数
     * @return 返回处理结果该结果将被下一个处理程序使用
     */
    default T result(T data) {
        return data;
    }

}
