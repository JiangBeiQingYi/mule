package net.lulihu.designPattern.chain;


/**
 * 建议责任链的形式化实现
 * <p>
 * 使用方式代码如下:
 * <pre>
 * public static class A extends ResponsibilityChain<AtomicInteger> {
 *  public boolean support(AtomicInteger data) {
 *   return true;
 * }
 *
 *  public boolean resolve(AtomicInteger data) {
 *   System.out.println(data.intValue());
 *   data.incrementAndGet();
 *   return true;
 *  }
 * }
 *
 * public static void main(String[] args) {
 *  ResponsibilityChain<AtomicInteger> chain = new A();
 *  chain.addHandler(new A()).addHandler(new A());
 *  chain.handler(new AtomicInteger(1));
 * }
 * </pre>
 * <p>
 * 注意：因为是以每个处理程序包含着下个处理程序的指针形成的责任模式，所有请使用第一个处理对象调用 handler方法
 */
public abstract class ResponsibilityChain<T> extends AbstractHandlerResponsibilityChainResolver<T> {

    /**
     * 持有下一个处理对象
     */
    private ResponsibilityChain<T> chain = null;

    public ResponsibilityChain(String name) {
        super(name);
    }

    /**
     * 设置下个处理对象
     *
     * @param chain 下个处理对象
     * @return 返回下个处理对象
     */
    public ResponsibilityChain<T> addHandler(ResponsibilityChain<T> chain) {
        this.chain = chain;
        return chain;
    }

    /**
     * 开始处理程序
     *
     * @param data 处理参数
     */
    public void handler(T data) throws Exception {
        try {
            data = handlerEvent(data);
            if (null != chain) chain.handler(data);
        } catch (ChainDisconnectedException ignored) {
            // 忽略
        }
    }

}