package net.lulihu.designPattern.chain;

import net.lulihu.ObjectKit.StrKit;

/**
 * 链条执行中发生可控的事件异常
 */
public class ChainEventException extends Exception {

    public ChainEventException(String message) {
        super(message);
    }

    public ChainEventException(String message, Object... objs) {
        this(StrKit.format(message, objs));
    }


    public ChainEventException(Throwable tx, String message) {
        super(message, tx);
    }

    public ChainEventException(Throwable tx, String message, Object... objs) {
        this(StrKit.format(message, objs), tx);
    }

}
