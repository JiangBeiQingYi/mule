package net.lulihu.designPattern.chain;

/**
 * 责任链事件处理器
 *
 * @param <T>
 */
public interface ResponsibilityChainEventHandler<T> {

    /**
     * 事件处理之前
     *
     * @param data 参数
     * @return 如果返回为true则继续执行，反之不执行
     */
    default boolean beforeEventHandler(T data) {
        return true;
    }

    /**
     * 在执行事件的过程中发送不可控制异常
     *
     * @param name 委托者名称
     * @param data 参数
     * @param ex   错误信息
     */
    void onEventExceptionHandler(String name, T data, Exception ex);


    /**
     * 事件处理之后
     *
     * @param data 参数
     */
    default void afterEventHandler(T data) {
    }

}
