package net.lulihu.http.okhttp;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.Buffer;
import okio.BufferedSink;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 自定义表单请求主体，请求参数不进行UTF-8码转换
 */
public class NoEncodeFormBody extends RequestBody {

    private final List<String> encodedNames;
    private final List<String> encodedValues;

    private static final MediaType CONTENT_TYPE = MediaType.parse("application/x-www-form-urlencoded");

    public NoEncodeFormBody(List<String> encodedNames, List<String> encodedValues) {
        this.encodedNames = Util.immutableList(encodedNames);
        this.encodedValues = Util.immutableList(encodedValues);
    }

    public int size() {
        return encodedNames.size();
    }

    public String encodedName(int index) {
        return encodedNames.get(index);
    }


    public String encodedValue(int index) {
        return encodedValues.get(index);
    }

    @Override
    public MediaType contentType() {
        return CONTENT_TYPE;
    }

    @Override
    public long contentLength() {
        return writeOrCountBytes(null, true);
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        writeOrCountBytes(sink, false);
    }

    /**
     * 将此请求写入信宿或测量其内容长度。
     * 我们有一种方法可以确保计数和内容的一致性，
     * 特别是当涉及到诸如测量标题字符串的编码长度或编码整数的数字长度等尴尬操作时。
     *
     * @param sink
     * @param countBytes
     * @return
     */
    private long writeOrCountBytes(BufferedSink sink, boolean countBytes) {
        long byteCount = 0L;
        Buffer buffer;
        if (countBytes) {
            buffer = new Buffer();
        } else {
            buffer = sink.buffer();
        }
        for (int i = 0, size = encodedNames.size(); i < size; i++) {
            if (i > 0) buffer.writeByte('&');
            buffer.writeUtf8(encodedNames.get(i));
            buffer.writeByte('=');
            buffer.writeUtf8(encodedValues.get(i));
        }
        if (countBytes) {
            byteCount = buffer.size();
            buffer.clear();
        }
        return byteCount;
    }

    public static class Builder {

        private final List<String> names = new ArrayList<>();
        private final List<String> values = new ArrayList<>();

        public NoEncodeFormBody.Builder add(String name, String value) {
            if (name == null) throw new NullPointerException("name == null");
            if (value == null) throw new NullPointerException("value == null");
            names.add(decode(name));
            values.add(decode(value));
            return this;
        }

        public NoEncodeFormBody build() {
            return new NoEncodeFormBody(names, values);
        }

        public String decode(String key) {
            return key;
//            try {
//                return URLEncoder.encode(key, "UTF-8");
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//                return key;
//            }
        }
    }


}
