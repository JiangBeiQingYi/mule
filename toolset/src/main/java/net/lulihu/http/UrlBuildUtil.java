package net.lulihu.http;


import com.alibaba.fastjson.JSON;
import net.lulihu.ObjectKit.BeanKit;

import java.util.Map;

public class UrlBuildUtil {


    /**
     * 构建get请求路径
     *
     * @param url   请求路径
     * @param param 参数
     */
    public static String buildGetUrl(String url, Map<String, ?> param) {
        return url + "?" + getParamsStr(param);
    }

    /**
     * get参数拼接
     *
     * @param param 参数
     */
    public static String getParamsStr(Map<String, ?> param) {
        StringBuilder sb = new StringBuilder();
        for (String key : param.keySet()) {
            sb.append("&").append(key).append("=");
            Object value = param.get(key);
            if (BeanKit.isPrimitive(value.getClass())) sb.append(value);
            else sb.append(JSON.toJSONString(value));
        }
        return sb.toString().replaceFirst("&", "");
    }
}
