package net.lulihu.disruptorKit;


import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.YieldingWaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.Assert;
import net.lulihu.ObjectKit.MapKit;
import net.lulihu.ObjectKit.NumberKit;
import net.lulihu.ObjectKit.ObjectKit;
import net.lulihu.ToolUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Disruptor管理
 */
@Slf4j
public class DisruptorManage {


    private static final Map<String, Disruptor> disruptions = new HashMap<>();

    // 将用于为消费者构造新线程的执行程序
    private ThreadFactory customerExecutor = Executors.defaultThreadFactory();

    static {
        // 添加关闭事件回调到JVM中
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            synchronized (disruptions) {
                if (MapKit.isNotEmpty(disruptions)) {
                    log.info("开始销毁 disruptor...");
                    Iterator<Map.Entry<String, Disruptor>> it = disruptions.entrySet().iterator();
                    while (it.hasNext()) {
                        shutdown(it);
                    }
                    System.out.println("disruptor 已经全部销毁完毕...");
                }
            }
        }));
    }

    private DisruptorManage() {
    }

    /**
     * 获取临时名称
     */
    public synchronized String getTemporaryName() {
        String randomString = ToolUtil.getRandomString(10);
        if (disruptions.containsKey(randomString)) {
            return getTemporaryName();
        }
        disruptions.put(randomString, null);
        return randomString;
    }

    /**
     * 根据名称关闭 disruption
     *
     * @param iterator 名称
     */
    private static void shutdown(Iterator<Map.Entry<String, Disruptor>> iterator) {
        Map.Entry<String, Disruptor> entry = iterator.next();
        iterator.remove();
        Disruptor disruptor = entry.getValue();
        if (ObjectKit.hasNotEmpty(disruptor)) {
            log.info("销毁disruption - {}", entry.getKey());
            disruptor.shutdown();
        }
    }

    /**
     * 根据名称关闭 disruption
     *
     * @param name 名称
     */
    public void shutdown(String name) {
        Disruptor disruptor = disruptions.remove(name);
        if (ObjectKit.hasNotEmpty(disruptor)) {
            log.info("销毁disruption - {}", name);
            disruptor.shutdown();
        }
    }

    /**
     * 获取实例
     */
    public static DisruptorManage getInstance() {
        return DisruptorEnum.INSTANCE.getDisruptorManage();
    }

    /**
     * 注册 Disruptor
     *
     * @param name         注册名称
     * @param eventFactory 事件执行工厂
     * @param bufferSize   指定环形缓冲区的大小，必须是2的幂. RingBuffer 大小，必须是 2 的 N 次方；
     * @param <T>          泛型
     * @return 注册成功 Disruptor
     */
    public <T> Disruptor<T> registered(String name, EventFactory<T> eventFactory, int bufferSize) {
        return registered(name, eventFactory, bufferSize, ProducerType.MULTI, new YieldingWaitStrategy());
    }

    /**
     * 注册 Disruptor
     *
     * @param name         注册名称
     * @param eventFactory 事件执行工厂
     * @param bufferSize   指定环形缓冲区的大小，必须是2的幂. RingBuffer 大小，必须是 2 的 N 次方；
     * @param producerType 定义生产者为单个还是多个
     * @param waitStrategy 定义消费者策略
     * @param <T>          泛型
     * @return 注册成功 Disruptor
     */
    public <T> Disruptor<T> registered(String name, EventFactory<T> eventFactory, int bufferSize,
                                       ProducerType producerType, WaitStrategy waitStrategy) {
        Assert.isTrue(disruptions.get(name) != null, "名称已存在!");
        log.info("注册disruption - {}", name);

        if (Integer.bitCount(bufferSize) != 1) {
            int old = bufferSize;
            bufferSize = NumberKit.getClosest2IndexGreaterThanSelf(old);
            log.warn("bufferSize【{}】不是2的幂，已经自动获取最近且最大2的指数次幂【{}】", old, bufferSize);
        }

        Disruptor<T> disruptor = new Disruptor<>(eventFactory, bufferSize, customerExecutor, producerType, waitStrategy);
        disruptions.put(name, disruptor);
        return disruptor;
    }

    /**
     * 利用枚举实现 单例
     */
    private enum DisruptorEnum {

        INSTANCE;

        DisruptorManage disruptorManage = new DisruptorManage();

        DisruptorEnum() {
        }

        public DisruptorManage getDisruptorManage() {
            return this.disruptorManage;
        }

    }


}
