package net.lulihu.disruptorKit.demo;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import net.lulihu.ObjectKit.StrKit;
import net.lulihu.designPattern.chain.AbstractHandlerResponsibilityChainResolver;
import net.lulihu.designPattern.chain.ChainEventException;
import net.lulihu.designPattern.chain.ResponsibilityChainHandler;
import net.lulihu.disruptorKit.DefaultEventFactory;
import net.lulihu.disruptorKit.DisruptorManage;
import net.lulihu.disruptorKit.Event;
import net.lulihu.disruptorKit.Producer;
import net.lulihu.disruptorKit.oneOf.WorkHandlerManage;

public class WorkHandlerDemo {

    public static void main(String[] args) {
        // 定义处理程序链
        ResponsibilityChainHandler<Integer> intHandler = new ResponsibilityChainHandler<>();
        intHandler.addHandler(new IntegerHandler(1))
                .addHandler(new IntegerHandler(2));

        ResponsibilityChainHandler<String> strHandler = new ResponsibilityChainHandler<>();
        strHandler.addHandler(new StringHandler(1))
                .addHandler(new StringHandler(2));

        // 注册责任链
        WorkHandlerManage handlerManage = WorkHandlerManage.getInstance();
        handlerManage.addWorkHandler(Integer.class, intHandler);
        handlerManage.addWorkHandler(String.class, strHandler);

        //注册 Disruptor
        DisruptorManage instance = DisruptorManage.getInstance();
        EventFactory<Event<Object>> eventFactory = DefaultEventFactory.factory();

        String name = "workHandlerTest";
        Disruptor<Event<Object>> eventDisruptor =
                instance.registered(name, eventFactory, 16,
                        ProducerType.MULTI, new BlockingWaitStrategy());
        // 生产者对象
        Producer<Object> producer = new Producer<>(eventDisruptor.getRingBuffer());

        // 定义消费者  不重复消费
        eventDisruptor.handleEventsWithWorkerPool(handlerManage.consumerNum(1));
        // 启动
        eventDisruptor.start();

        for (int i = 0; i < 5; i++) {
            if (i % 2 == 0) {
                producer.submit(i);
            } else {
                producer.submit("o");
            }
        }

        instance.shutdown(name);
    }

    /**
     * 整数处理程序
     */
    public static class IntegerHandler extends AbstractHandlerResponsibilityChainResolver<Integer> {

        private int number;

        public IntegerHandler(int number) {
            super("整数类型处理者");
            this.number = number;
        }

        @Override
        public void resolve(Integer data) throws ChainEventException {
            System.out.println(StrKit.format("整数类型处理者-{}, 处理参数:{}", number, data));
        }

        @Override
        public Integer result(Integer data) {
            return ++data;
        }
    }

    /**
     * 字符串处理程序
     */
    public static class StringHandler extends AbstractHandlerResponsibilityChainResolver<String> {

        private int number;

        public StringHandler(int number) {
            super("字符类型处理者");
            this.number = number;
        }

        @Override
        public void resolve(String data) throws ChainEventException {
            System.out.println(StrKit.format("字符类型处理者-{}, 处理参数:{}", number, data));
        }

        @Override
        public String result(String data) {
            return data += "i";
        }
    }

}
