package net.lulihu.disruptorKit;

import com.lmax.disruptor.EventFactory;

/**
 * 默认事件工厂
 */
public class DefaultEventFactory {

    public static <T> EventFactory<Event<T>> factory() {
        return Event::new;
    }
}
