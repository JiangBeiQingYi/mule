package net.lulihu;


import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.CollectionKit;
import net.lulihu.ObjectKit.ObjectKit;
import net.lulihu.ObjectKit.StrKit;
import net.lulihu.exception.ExceptionEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * 断言方法
 */
@Slf4j
public class Assert {
    /**
     * 如果为true 则抛出异常
     *
     * @param boo           判断
     * @param exceptionEnum 异常信息
     */
    public static void isTrue(boolean boo, ExceptionEnum exceptionEnum) {
        if (boo)
            throw new IllegalArgumentException(exceptionEnum.getMessage());
    }

    /**
     * 如果为true 则抛出异常
     *
     * @param boo 判断
     * @param e   异常信息
     */
    public static void isTrueExcep(boolean boo, RuntimeException e) {
        if (boo)
            throw e;
    }


    /**
     * 如果为true 则抛出异常
     *
     * @param boo     判断
     * @param message 异常信息
     */
    public static void isTrue(boolean boo, String message) {
        if (boo)
            throw new IllegalArgumentException(message);
    }

    /**
     * 断言一个对象不能为null
     *
     * @param object  断言对象
     * @param message 异常message
     */
    public static void notNull(Object object, String message) {
        notNull(object, new NullPointerException(message));
    }

    /**
     * 断言一个对象不能为null
     *
     * @param object 断言对象
     */
    public static void notNull(Object object) {
        notNull(object, new NullPointerException());
    }

    /**
     * 断言一个对象不能为null
     *
     * @param object 断言对象
     * @param e      异常
     */
    public static void notNull(Object object, RuntimeException e) {
        isTrueExcep(object == null, e);
    }

    /**
     * 断言一个对象不能为空
     *
     * @param object  断言对象
     * @param message 异常message
     */
    public static void notEmpty(Object object, String message) {
        notEmpty(object, new IllegalArgumentException(message));
    }

    /**
     * 断言一个对象不能为空
     *
     * @param object 断言对象
     * @param e      异常
     */
    public static void notEmpty(Object object, RuntimeException e) {
        isTrueExcep(ObjectKit.hasEmpty(object), e);
    }

    /**
     * 断言一个字符串不可以为空或者 null 否则抛出异常
     *
     * @param str     断言字符串
     * @param message 异常message
     */
    public static void notNull(String str, String message) {
        isTrue(StrKit.isEmpty(str), message);
    }

    /**
     * 断言数组包含元素; 也就是说，它不能为空，并且必须包含至少一个元素。
     *
     * @param array   数组
     * @param message 异常message
     */
    public static void notEmpty(Object[] array, String message) {
        isTrue(CollectionKit.isEmpty(array), message);
    }

    /**
     * 断言class文件存在
     *
     * @param fullClassName class对象全类名
     * @param message       异常message
     */
    public static void foundClass(String fullClassName, String message) {
        try {
            Class.forName(fullClassName);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(message, e);
        }
    }

    /**
     * 断言字符串中不可以有特殊字符
     *
     * @param str     字符串
     * @param message 提示消息 可以使用{}作为占位符替换错误字符串
     * @param symbols 特殊字符
     */
    public static void noSpecialSymbols(String str, String message, String... symbols) {
        List<String> err = new ArrayList<>();
        for (String sym : symbols) {
            if (str.contains(sym)) {
                err.add(sym);
            }
        }
        if (err.size() != 0) {
            throw new IllegalArgumentException(message.replace("{}", "【" + String.join(",", err) + "】"));
        }
    }

}
