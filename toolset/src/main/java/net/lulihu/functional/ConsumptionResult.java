package net.lulihu.functional;

@FunctionalInterface
public interface ConsumptionResult<T> {

    /**
     * 接收指定表达式，等待执行
     */
    T accept();

}
