package net.lulihu.functional;

import java.lang.reflect.Field;

@FunctionalInterface
public interface FieldValueProvider {

    /**
     * 获取值
     *
     * @param field 参数类型
     * @return 对应参数名的值
     */
    Object value(Field field);
}