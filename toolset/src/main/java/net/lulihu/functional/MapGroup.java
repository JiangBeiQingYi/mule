package net.lulihu.functional;

import java.util.Map;

/**
 * 集合对象分组接口 方便使用表达式执行
 */
@FunctionalInterface
public interface MapGroup<O, K, T> {

    /**
     * 对象分组封装
     *
     * @param obj    对象
     * @param result 封装结果
     */
    void accept(O obj, Map<K, T> result);

}