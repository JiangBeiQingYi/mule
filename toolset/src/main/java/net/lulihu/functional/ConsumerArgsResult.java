package net.lulihu.functional;

@SuppressWarnings("unchecked")
@FunctionalInterface
public interface ConsumerArgsResult<E, T> {

    /**
     * 接收指定表达式，等待执行
     */
    T accept(E... args);


}
