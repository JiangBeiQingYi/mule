package net.lulihu.functional;

import java.util.Collection;

@FunctionalInterface
public interface CollectionConsumption<T> {

    /**
     * 接收指定表达式，等待执行
     */
    Collection<T> accept();

}
