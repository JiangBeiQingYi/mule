package net.lulihu.ObjectKit;

import com.alibaba.fastjson.JSON;
import net.lulihu.exception.ToolBoxException;
import net.lulihu.functional.ConsumerResult;
import net.lulihu.functional.FieldValueProvider;
import net.lulihu.functional.ValueProvider;

import java.beans.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Bean工具类
 */
public class BeanKit {

    private BeanKit() {

    }

    /**
     * bean字段缓存 空间换时间
     */
    private final static Map<Class<?>, Map<String, String>> beanPropertyNameMap = new ConcurrentHashMap<>();

    /**
     * 获取对象的属性名称，并且对属性名称进行驼峰与下划线模式复制
     *
     * @param clazz clazz
     * @return key->原对象属性名称的驼峰或者下划线模式  value 原对象属性名称
     */
    public static Map<String, String> getBeanPropertyName(Class<?> clazz) {
        if (beanPropertyNameMap.containsKey(clazz)) return beanPropertyNameMap.get(clazz);

        List<Field> fields = ReflectKit.getAllFieldsList(clazz);
        if (CollectionKit.isEmpty(fields)) {
            Map<String, String> result = new HashMap<>();
            beanPropertyNameMap.put(clazz, result);
            return result;
        }

        Map<String, String> result = new HashMap<>(fields.size() * 2);
        for (Field field : fields) {
            String name = field.getName();
            result.put(StrKit.toCamelCase(name), name);
        }
        // 避免出现 ConcurrentModificationException
        Map<String, String> resultTmp = new HashMap<>(fields.size());
        for (Entry<String, String> next : result.entrySet()) {
            resultTmp.put(StrKit.toUnderlineCase(next.getKey()), next.getValue());
        }
        result.putAll(resultTmp);
        beanPropertyNameMap.put(clazz, result);
        return result;
    }

    /**
     * bean字段缓存 空间换时间
     */
    private final static Map<Class<?>, List<Field>> beanFieldMap = new ConcurrentHashMap<>();

    /**
     * 获取bean的所有字段
     *
     * @param clazz 对象类型
     * @return 对象字段
     */
    public static List<Field> getBeanField(Class<?> clazz) {
        if (beanFieldMap.containsKey(clazz)) return beanFieldMap.get(clazz);
        List<Field> fields = ReflectKit.getAllFieldsList(clazz);
        beanFieldMap.put(clazz, fields);
        return fields;
    }

    /**
     * 判断是否为Bean对象
     *
     * @param clazz 待测试类
     * @return 是否为Bean对象
     */
    public static boolean isBean(Class<?> clazz) {
        if (ClassKit.isNormalClass(clazz)) {
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                if (method.getParameterTypes().length == 1 && method.getName().startsWith("set")) {
                    //检测包含标准的setXXX方法即视为标准的JavaBean
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 是否为基础数据类型 包括String类型
     *
     * @return 当且仅当此类表示基本类型或者String类型时才返回true
     */
    public static boolean isPrimitive(Class<?> clazz) {
        return String.class.equals(clazz)
                || BasicType.wrapperPrimitiveMap.containsKey(clazz)
                || BasicType.primitiveWrapperMap.containsKey(clazz);
    }

    /**
     * 是否为基础数值数据类型
     *
     * @return 当且仅当此类表示基本数值类型时才返回true
     */
    public static boolean isNumberPrimitive(Class<?> clazz) {
        return BasicType.numberWrapperPrimitiveMap.containsKey(clazz)
                || BasicType.numberPrimitiveWrapperMap.containsKey(clazz);
    }


    public static PropertyEditor findEditor(Class<?> type) {
        return PropertyEditorManager.findEditor(type);
    }

    /**
     * 获得Bean字段描述数组
     *
     * @param clazz Bean类
     * @return 字段描述数组
     * @throws IntrospectionException
     */
    public static PropertyDescriptor[] getPropertyDescriptors(Class<?> clazz) throws IntrospectionException {
        return Introspector.getBeanInfo(clazz).getPropertyDescriptors();
    }

    /**
     * 获得字段名和字段描述Map
     *
     * @param clazz Bean类
     * @return 字段名和字段描述Map
     * @throws IntrospectionException
     */
    public static Map<String, PropertyDescriptor> getFieldNamePropertyDescriptorMap(Class<?> clazz) throws IntrospectionException {
        final PropertyDescriptor[] propertyDescriptors = getPropertyDescriptors(clazz);
        Map<String, PropertyDescriptor> map = new HashMap<>();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            map.put(propertyDescriptor.getName(), propertyDescriptor);
        }
        return map;
    }

    /**
     * 获得Bean类属性描述
     *
     * @param clazz     Bean类
     * @param fieldName 字段名
     * @return PropertyDescriptor
     * @throws IntrospectionException
     */
    public static PropertyDescriptor getPropertyDescriptor(Class<?> clazz, final String fieldName) throws IntrospectionException {
        PropertyDescriptor[] propertyDescriptors = getPropertyDescriptors(clazz);
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            if (ObjectKit.equals(fieldName, propertyDescriptor.getName())) {
                return propertyDescriptor;
            }
        }
        return null;
    }

    /**
     * Map转换为Bean对象
     *
     * @param map       Map
     * @param beanClass Bean Class
     * @return Bean
     */
    public static <T> T mapToBean(Map<?, ?> map, Class<T> beanClass) {
        return fillBeanWithMap(map, ClassKit.newInstance(beanClass));
    }

    /**
     * Map转换为Bean对象<br>
     * 忽略大小写
     *
     * @param map       Map
     * @param beanClass Bean Class
     * @return Bean
     */
    public static <T> T mapToBeanIgnoreCase(Map<?, ?> map, Class<T> beanClass) {
        return fillBeanWithMapIgnoreCase(map, ClassKit.newInstance(beanClass));
    }

    /**
     * 使用Map填充Bean对象
     *
     * @param map  Map
     * @param bean Bean
     * @return Bean
     */
    public static <T> T fillBeanWithMap(final Map<?, ?> map, T bean) {
        return fillBean(bean, (name, clazz) -> map.get(name));
    }

    /**
     * 使用Map填充Bean对象，可配置将下划线转换为驼峰
     *
     * @param map           Map
     * @param bean          Bean
     * @param isToCamelCase 是否将下划线模式转换为驼峰模式
     * @return Bean
     */
    public static <T> T fillBeanWithMap(Map<?, ?> map, T bean, boolean isToCamelCase) {
        if (isToCamelCase) {
            final Map<Object, Object> map2 = new HashMap<>();
            for (Entry<?, ?> entry : map.entrySet()) {
                final Object key = entry.getKey();
                if (key instanceof String) {
                    final String keyStr = (String) key;
                    map2.put(StrKit.toCamelCase(keyStr), entry.getValue());
                } else {
                    map2.put(key, entry.getValue());
                }
            }
            return fillBeanWithMap(map2, bean);
        }

        return fillBeanWithMap(map, bean);
    }

    /**
     * 使用Map填充Bean对象，忽略大小写
     *
     * @param map  Map
     * @param bean Bean
     * @return Bean
     */
    public static <T> T fillBeanWithMapIgnoreCase(Map<?, ?> map, T bean) {
        final Map<Object, Object> map2 = new HashMap<>();
        for (Entry<?, ?> entry : map.entrySet()) {
            final Object key = entry.getKey();
            if (key instanceof String) {
                final String keyStr = (String) key;
                map2.put(keyStr.toLowerCase(), entry.getValue());
            } else {
                map2.put(key, entry.getValue());
            }
        }
        return fillBean(bean, (name, clazz) -> map2.get(name.toLowerCase()));
    }

    /**
     * ServletRequest 参数转Bean
     *
     * @param request
     * @param beanClass
     * @return Bean
     */
    public static <T> T requestParamToBean(javax.servlet.ServletRequest request, Class<T> beanClass) {
        return fillBeanWithRequestParam(request, ClassKit.newInstance(beanClass));
    }

    /**
     * ServletRequest 参数转Bean
     *
     * @param request ServletRequest
     * @param bean    目标bean
     * @return Bean
     */
    public static <T> T fillBeanWithRequestParam(final javax.servlet.ServletRequest request, T bean) {
        final String beanName = StrKit.lowerFirst(bean.getClass().getSimpleName());
        return fillBean(bean, (name, clazz) -> {
            String value = request.getParameter(name);
            if (StrKit.isEmpty(value)) {
                // 使用类名前缀尝试查找值
                value = request.getParameter(beanName + StrKit.DOT + name);
                if (StrKit.isEmpty(value)) {
                    // 此处取得的值为空时跳过，包括null和""
                    value = null;
                }
            }
            return value;
        });
    }

    /**
     * ServletRequest 参数转Bean
     *
     * @param <T>
     * @param beanClass     Bean Class
     * @param valueProvider 值提供者
     * @return Bean
     */
    public static <T> T toBean(Class<T> beanClass, ValueProvider valueProvider) {
        return fillBean(ClassKit.newInstance(beanClass), valueProvider);
    }

    /**
     * 填充Bean
     *
     * @param <T>
     * @param bean          Bean
     * @param valueProvider 值提供者
     * @return Bean
     */
    public static <T> T fillBean(T bean, ValueProvider valueProvider) {
        if (null == valueProvider) return bean;

        Object value;
        List<Field> fields = getBeanField(bean.getClass());
        for (Field field : fields) {
            String fieldName = field.getName();
            value = valueProvider.value(fieldName, field.getType());
            if (ObjectKit.hasEmpty(value)) continue;
            ReflectKit.setFieldValue(bean, fieldName, value);
        }
        return bean;
    }

    /**
     * 填充Bean属性
     *
     * @param <T>
     * @param bean          Bean
     * @param valueProvider 值提供者
     * @return Bean
     */
    public static <T> T fillBeanField(T bean, FieldValueProvider valueProvider) {
        if (null == valueProvider) return bean;

        Object value;
        List<Field> fields = getBeanField(bean.getClass());
        for (Field field : fields) {
            String fieldName = field.getName();
            value = valueProvider.value(field);
            if (ObjectKit.hasEmpty(value)) continue;
            ReflectKit.setFieldValue(bean, fieldName, value);
        }
        return bean;
    }

    /**
     * 对象转Map
     *
     * @param bean bean对象
     * @return Map
     */
    public static <T> Map<String, Object> beanToMap(T bean, Map<String, Object> map) {
        return beanToMap(bean, map, false);
    }

    /**
     * 对象转Map
     *
     * @param bean bean对象
     * @return Map
     */
    public static <T> Map<String, String> beanToMap(Map<String, String> map, T bean) {
        return beanToMap(bean, map, false, value -> {
            if (BeanKit.isPrimitive(value.getClass())) return value.toString();
            return JSON.toJSONString(value);
        });
    }

    /**
     * 对象转Map
     *
     * @param bean bean对象
     * @return Map
     */
    public static <T> List<Map<String, Object>> listToMapList(List<T> bean, Map<String, Object> map) {
        ArrayList<Map<String, Object>> maps = new ArrayList<>();
        for (T t : bean) {
            maps.add(beanToMap(bean, map, false));
        }
        return maps;
    }

    /**
     * 对象转Map
     *
     * @param bean              bean对象
     * @param isToUnderlineCase 是否转换为下划线模式
     * @return Map
     */
    public static <T> Map<String, Object> beanToMap(T bean, Map<String, Object> map, boolean isToUnderlineCase) {
        return beanToMap(bean, map, isToUnderlineCase, value -> value);
    }

    /**
     * 对象转Map
     *
     * @param bean              bean对象
     * @param map               返回值map对象
     * @param isToUnderlineCase 是否转换为下划线模式
     * @param consumerResult    封装map value值的表达式类型
     * @param <T>               泛型bean对象
     * @param <D>               泛型返回值value对象
     * @return map
     */
    public static <T, D> Map<String, D> beanToMap(T bean, Map<String, D> map, boolean isToUnderlineCase,
                                                  ConsumerResult<Object, D> consumerResult) {
        if (bean == null) return null;
        try {
            final PropertyDescriptor[] propertyDescriptors = getPropertyDescriptors(bean.getClass());
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                // 过滤class属性
                if (!key.equals("class")) {
                    // 得到property对应的getter方法
                    Method getter = property.getReadMethod();
                    Object value = getter.invoke(bean);
                    if (null != value) {
                        map.put(isToUnderlineCase ? StrKit.toUnderlineCase(key) : key, consumerResult.accept(value));
                    }
                }
            }
        } catch (Exception e) {
            throw new ToolBoxException(e);
        }
        return map;
    }

    /**
     * 复制Bean对象属性
     *
     * @param source 源Bean对象
     * @param target 目标Bean对象
     */
    public static void copyProperties(Object source, Object target) {
        copyProperties(source, target, CopyOptions.create());
    }

    /**
     * 复制Bean对象属性<br>
     * 限制类用于限制拷贝的属性，例如一个类我只想复制其父类的一些属性，就可以将editable设置为父类
     *
     * @param source           源Bean对象
     * @param target           目标Bean对象
     * @param ignoreProperties 不拷贝的的属性列表
     */
    public static void copyProperties(Object source, Object target, String... ignoreProperties) {
        copyProperties(source, target, CopyOptions.create().setIgnoreProperties(ignoreProperties));
    }

    /**
     * 复制Bean对象属性<br>
     * 限制类用于限制拷贝的属性，例如一个类我只想复制其父类的一些属性，就可以将editable设置为父类
     *
     * @param source      源Bean对象
     * @param target      目标Bean对象
     * @param copyOptions 拷贝选项，见 {@link CopyOptions}
     */
    public static void copyProperties(Object source, Object target, CopyOptions copyOptions) {
        if (null == copyOptions) {
            copyOptions = new CopyOptions();
        }

        Class<?> actualEditable = target.getClass();
        if (copyOptions.editable != null) {
            //检查限制类是否为target的父类或接口
            if (!copyOptions.editable.isInstance(target)) {
                throw new IllegalArgumentException(StrKit.format("目标类[{}]无法分配给可编辑类[{}]", target.getClass().getName(), copyOptions.editable.getName()));
            }
            actualEditable = copyOptions.editable;
        }
        PropertyDescriptor[] targetPds;
        Map<String, PropertyDescriptor> sourcePdMap;
        try {
            sourcePdMap = getFieldNamePropertyDescriptorMap(source.getClass());
            targetPds = getPropertyDescriptors(actualEditable);
        } catch (IntrospectionException e) {
            throw new ToolBoxException(e);
        }

        HashSet<String> ignoreSet = copyOptions.ignoreProperties != null ? CollectionKit.newHashSet(copyOptions.ignoreProperties) : null;
        for (PropertyDescriptor targetPd : targetPds) {
            Method writeMethod = targetPd.getWriteMethod();
            if (writeMethod != null && (ignoreSet == null || !ignoreSet.contains(targetPd.getName()))) {
                PropertyDescriptor sourcePd = sourcePdMap.get(targetPd.getName());
                if (sourcePd != null) {
                    Method readMethod = sourcePd.getReadMethod();
                    // 源对象字段的getter方法返回值必须可转换为目标对象setter方法的第一个参数
                    if (readMethod != null && ClassKit.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                        try {
                            Object value = ClassKit.setAccessible(readMethod).invoke(source);
                            if (null != value || !copyOptions.isIgnoreNullValue) {
                                ClassKit.setAccessible(writeMethod).invoke(target, value);
                            }
                        } catch (Throwable ex) {
                            throw new ToolBoxException(ex, "将属性[{}]复制到[{}]错误: {}", sourcePd.getName(), targetPd.getName(), ex.getMessage());
                        }
                    }
                }
            }
        }
    }

    /**
     * 属性拷贝选项<br>
     * 包括：<br>
     * 1、限制的类或接口，必须为目标对象的实现接口或父类，用于限制拷贝的属性，
     * 例如一个类我只想复制其父类的一些属性，就可以将editable设置为父类<br>
     * 2、是否忽略空值，当源对象的值为null时，true: 忽略而不注入此值，false: 注入null<br>
     * 3、忽略的属性列表，设置一个属性列表，不拷贝这些属性值<br>
     *
     * @author Looly
     */
    public static class CopyOptions {
        /**
         * 限制的类或接口，必须为目标对象的实现接口或父类，用于限制拷贝的属性，例如一个类我只想复制其父类的一些属性，就可以将editable设置为父类
         */
        private Class<?> editable;
        /**
         * 是否忽略空值，当源对象的值为null时，true: 忽略而不注入此值，false: 注入null
         */
        private boolean isIgnoreNullValue;
        /**
         * 忽略的属性列表，设置一个属性列表，不拷贝这些属性值
         */
        private String[] ignoreProperties;

        /**
         * 创建拷贝选项
         *
         * @return 拷贝选项
         */
        public static CopyOptions create() {
            return new CopyOptions();
        }

        /**
         * 创建拷贝选项
         *
         * @param editable          限制的类或接口，必须为目标对象的实现接口或父类，用于限制拷贝的属性
         * @param isIgnoreNullValue 是否忽略空值，当源对象的值为null时，true: 忽略而不注入此值，false: 注入null
         * @param ignoreProperties  忽略的属性列表，设置一个属性列表，不拷贝这些属性值
         * @return 拷贝选项
         */
        public static CopyOptions create(Class<?> editable, boolean isIgnoreNullValue, String... ignoreProperties) {
            return new CopyOptions(editable, isIgnoreNullValue, ignoreProperties);
        }

        /**
         * 构造拷贝选项
         */
        public CopyOptions() {
        }

        /**
         * 构造拷贝选项
         *
         * @param editable          限制的类或接口，必须为目标对象的实现接口或父类，用于限制拷贝的属性
         * @param isIgnoreNullValue 是否忽略空值，当源对象的值为null时，true: 忽略而不注入此值，false: 注入null
         * @param ignoreProperties  忽略的属性列表，设置一个属性列表，不拷贝这些属性值
         */
        public CopyOptions(Class<?> editable, boolean isIgnoreNullValue, String... ignoreProperties) {
            this.editable = editable;
            this.isIgnoreNullValue = isIgnoreNullValue;
            this.ignoreProperties = ignoreProperties;
        }

        /**
         * 设置限制的类或接口，必须为目标对象的实现接口或父类，用于限制拷贝的属性
         *
         * @param editable 限制的类或接口
         * @return CopyOptions
         */
        public CopyOptions setEditable(Class<?> editable) {
            this.editable = editable;
            return this;
        }

        /**
         * 设置是否忽略空值，当源对象的值为null时，true: 忽略而不注入此值，false: 注入null
         *
         * @param isIgnoreNullVall 是否忽略空值，当源对象的值为null时，true: 忽略而不注入此值，false: 注入null
         * @return CopyOptions
         */
        public CopyOptions setIgnoreNullValue(boolean isIgnoreNullVall) {
            this.isIgnoreNullValue = isIgnoreNullVall;
            return this;
        }

        /**
         * 设置忽略的属性列表，设置一个属性列表，不拷贝这些属性值
         *
         * @param ignoreProperties 忽略的属性列表，设置一个属性列表，不拷贝这些属性值
         * @return CopyOptions
         */
        public CopyOptions setIgnoreProperties(String... ignoreProperties) {
            this.ignoreProperties = ignoreProperties;
            return this;
        }
    }
}
