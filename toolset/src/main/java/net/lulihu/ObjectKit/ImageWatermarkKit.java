package net.lulihu.ObjectKit;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 图片水印操作方法类
 */
public class ImageWatermarkKit {

    private static int R = 255, G = 255, B = 255; //Color对象的三原色属性

    /**
     * 文件转图片
     *
     * @param outImgPath       添加水印后图片输出路径
     * @param fontColor        水印文字的颜色
     * @param fontSize         文字大小
     * @param fontName         字体名称
     * @param imgWidth         图片高度
     * @param imgHeight        图片宽度
     * @param waterMarkContent 水印的文字
     */
    public static void strToImage(String outImgPath, Color fontColor, int fontSize,
                                  String fontName, int imgWidth, int imgHeight,
                                  String waterMarkContent) throws IOException {

        BufferedImage bufImg = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bufImg.createGraphics();
        g.drawImage(bufImg, 0, 0, imgWidth, imgHeight, null);
        // 设置背景色
        g.setColor(new Color(R, G, B));
        // 填充矩形
        g.fillRect(0, 0, imgWidth, imgHeight);
        g.setFont(new Font(fontName, Font.PLAIN, fontSize)); // 设置字体
        g.setColor(fontColor);// 设置字体颜色
        // 写入水印
        automaticLineBreak(g, waterMarkContent, 0, imgWidth, imgHeight, fontSize);
        g.dispose();

        // 输出图片
        try (FileOutputStream outImgStream = new FileOutputStream(outImgPath)) {
            ImageIO.write(bufImg, "jpg", outImgStream);
            outImgStream.flush();
        }
    }

    /**
     * 图片添加水印
     *
     * @param srcImgPath       需要添加水印的图片的路径
     * @param outImgPath       添加水印后图片输出路径
     * @param markContentColor 水印文字的颜色
     * @param fontSize         文字大小
     * @param waterMarkContent 水印的文字
     * @param fontName         字体名称
     */
    public static void waterPress(String srcImgPath, String outImgPath,
                                  Color markContentColor, int fontSize,
                                  String waterMarkContent, String fontName) throws IOException {
        // 读取原图片信息
        File srcImgFile = new File(srcImgPath);
        Image srcImg = ImageIO.read(srcImgFile);
        int srcImgWidth = srcImg.getWidth(null);
        int srcImgHeight = srcImg.getHeight(null);
        // 加水印
        BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bufImg.createGraphics();
        g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
        Font font = new Font(fontName, Font.PLAIN, fontSize);
        g.setColor(markContentColor);//根据图片的背景设置水印颜色
        g.setFont(font);
        // 写入水印
        automaticLineBreak(g, waterMarkContent, 0, srcImgWidth, srcImgHeight, fontSize);
        g.dispose();

        // 输出图片
        try (FileOutputStream outImgStream = new FileOutputStream(outImgPath)) {
            ImageIO.write(bufImg, "jpg", outImgStream);
            outImgStream.flush();
        }
    }

    /**
     * 写入水印文字，并且自动换行
     *
     * @param g                绘图对象
     * @param waterMarkContent 文字
     * @param x                写入水印的每行开始位置
     * @param srcImgWidth      图片长度
     * @param srcImgHeight     图片高度
     * @param fontSize         字体大小
     */
    private static void automaticLineBreak(Graphics2D g, String waterMarkContent,
                                           int x, int srcImgWidth, int srcImgHeight, int fontSize) {
        int tempCharLen;//单字符长度
        int tempLineLen = 0;//单行字符总长度临时计算

        int fontLen = getWatermarkLength(waterMarkContent, g);
        int line = fontLen / srcImgWidth;//文字长度相对于图片宽度应该有多少行
        //文字叠加,自动换行叠加
        int tempY = srcImgHeight - (line + 1) * fontSize;

        StringBuilder sb = new StringBuilder();
        for (int i = 0, len = waterMarkContent.length(); i < len; i++) {
            char tempChar = waterMarkContent.charAt(i);
            tempCharLen = getCharLen(tempChar, g);
            tempLineLen += tempCharLen;
            if (tempLineLen >= srcImgWidth) {
                //长度已经满一行,进行文字叠加
                g.drawString(sb.toString(), x, tempY);
                sb.delete(x, sb.length());//清空内容,重新追加
                tempY += fontSize;
                tempLineLen = 0;
            }
            sb.append(tempChar);//追加字符
        }
        g.drawString(sb.toString(), 0, tempY);//最后叠加余下的文字
    }

    /**
     * 获取写入字符所占用的长度
     *
     * @param c 字符
     * @param g 绘图对象
     */
    private static int getCharLen(char c, Graphics2D g) {
        return g.getFontMetrics(g.getFont()).charWidth(c);
    }

    /**
     * 获取水印文字总长度
     *
     * @param waterMarkContent 水印的文字
     * @param g                绘图对象
     * @return 水印文字总长度
     */
    private static int getWatermarkLength(String waterMarkContent, Graphics2D g) {
        return g.getFontMetrics(g.getFont()).charsWidth(waterMarkContent.toCharArray(), 0, waterMarkContent.length());
    }

}