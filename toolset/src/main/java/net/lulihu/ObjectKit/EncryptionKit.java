package net.lulihu.ObjectKit;

import org.apache.commons.codec.binary.Base64;
import net.lulihu.Assert;

import java.security.MessageDigest;

/**
 * 加密工具类
 */
public class EncryptionKit {

    /**
     * base64解码
     *
     * @param bytes 解码字符串的字节数组
     */
    public static byte[] base64Decode(byte[] bytes) {
        return Base64.decodeBase64(bytes);
    }

    /**
     * base64编码
     *
     * @param bytes 被加密字符串的字节数组
     */
    public static String base64Encode(byte[] bytes) {
        return Base64.encodeBase64String(bytes);
    }

    /**
     * md5编码
     *
     * @param bytes       被加密字符串的字节数组
     * @param toLowerCase <code>true</code> 转换成小写格式 ， <code>false</code> 转换成大写格式
     */
    public static String md5Encode(byte[] bytes, boolean toLowerCase) {
        return encode("MD5", bytes, toLowerCase);
    }

    /**
     * sha1编码
     *
     * @param bytes       被加密字符串的字节数组
     * @param toLowerCase <code>true</code> 转换成小写格式 ， <code>false</code> 转换成大写格式
     */
    public static String sha1Encode(byte[] bytes, boolean toLowerCase) {
        return encode("SHA1", bytes, toLowerCase);
    }

    /**
     * 使用直接算法进行字符串加密
     *
     * @param algorithm   请求的算法的名称。 有关标准算法名称的信息，
     *                    请参阅Java Cryptography体系结构标准算法名称文档中的MessageDigest部分。
     * @param bytes       被加密字符串的字节数组
     * @param toLowerCase <code>true</code> 转换成小写格式 ， <code>false</code> 转换成大写格式
     * @return 加密字符串
     */
    public static String encode(String algorithm, byte[] bytes, boolean toLowerCase) {
        Assert.isTrue(bytes == null, "加密字符串不可以为NULL");
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.update(bytes);
            return HexKit.encodeHexStr(messageDigest.digest(), toLowerCase);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
