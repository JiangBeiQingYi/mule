package net.lulihu.ObjectKit;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 简单的线程副本缓存工具
 */
public class ConcurrentThreadLocalKit {

    private final Map<Thread, Map<String, Object>> concurrentThreadLocal;


    /**
     * 构造函数私有化
     */
    private ConcurrentThreadLocalKit() {
        this.concurrentThreadLocal = new ConcurrentHashMap<>();
    }

    /**
     * 获取实例
     *
     * @return 实例对象
     */
    public static ConcurrentThreadLocalKit getInstance() {
        return ThreadLocalSingleEnum.INSTANCE.getThreadLocalKit();
    }

    /**
     * 判断 指定的键是否存在于当前的线程副本中
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean containsKey(String key) {
        return getCurrentThreadLocal().containsKey(key);
    }

    /**
     * 设置副本信息
     *
     * @param key 数据唯一键
     * @param obj 事务上下文
     */
    public void set(String key, Object obj) {
        getCurrentThreadLocal().put(key, obj);
    }

    /**
     * 获取数据枚举获取对应副本信息
     *
     * @param key 数据唯一键
     */
    public Object get(String key) {
        return getCurrentThreadLocal().get(key);
    }

    /**
     * 删除当前线程的指定数据副本
     *
     * @param key 数据唯一键
     */
    public void clear(String key) {
        getCurrentThreadLocal().remove(key);
    }

    /**
     * 删除当前线程的所有数据副本
     */
    public void clear() {
        concurrentThreadLocal.remove(Thread.currentThread());
    }

    /**
     * 获取当前线程副本信息
     *
     * @return 副本信息
     */
    private Map<String, Object> getCurrentThreadLocal() {
        return concurrentThreadLocal.computeIfAbsent(Thread.currentThread(), k -> new HashMap<>());
    }

    private enum ThreadLocalSingleEnum {

        INSTANCE;

        private ConcurrentThreadLocalKit threadLocalKit;

        ThreadLocalSingleEnum() {
            this.threadLocalKit = new ConcurrentThreadLocalKit();
        }

        public ConcurrentThreadLocalKit getThreadLocalKit() {
            return threadLocalKit;
        }
    }

}
