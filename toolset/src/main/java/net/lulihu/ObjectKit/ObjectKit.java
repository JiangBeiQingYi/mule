package net.lulihu.ObjectKit;

import net.lulihu.exception.ToolBoxException;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * 一些通用的函数
 */
public class ObjectKit {

    private ObjectKit() {
    }


    /**
     * 如果为空, 则调用默认值 <br>
     * 空参数的定义如下<br>
     * 1、对象不为null <br>
     * 2、String 不为"" or " " <br>
     * 3、List,Set,Map,Object[],int[],long[] 长度大于0
     *
     * @param obj          被判断的参数
     * @param defaultValue 为空默认值
     */
    public static Object nullToDefault(Object obj, Object defaultValue) {
        return hasEmpty(obj) ? defaultValue : obj;
    }

    /**
     * 比较两个对象是否相等。<br>
     * 相同的条件有两个，满足其一即可：<br>
     * 1. obj1 == null && obj2 == null; 2. obj1.equals(obj2)
     *
     * @param obj1 对象1
     * @param obj2 对象2
     * @return 是否相等
     */
    public static boolean equals(Object obj1, Object obj2) {
        return Objects.equals(obj1, obj2);
    }

    /**
     * 对象是否不为空
     * <p>
     * 空参数的定义如下<br>
     * 1、String 不为"" or " " <br>
     * 2、List,Set,Map,Object[],int[],long[] 长度大于0
     *
     * @param o 校验对象
     * @return 为空则为false  反之true
     */
    public static boolean hasNotEmpty(Object o) {
        return !hasEmpty(o);
    }

    /**
     * 对象是否为空
     * <p>
     * 空参数的定义如下<br>
     * 1、对象不为null
     * 2、String 不为"" or " " <br>
     * 3、List,Set,Map,Object[],int[],long[] 长度大于0
     *
     * @param o 校验对象
     * @return 为空则为true  反之false
     */
    @SuppressWarnings("rawtypes")
    public static boolean hasEmpty(Object o) {
        if (o == null) {
            return true;
        }
        if (o instanceof String) {
            if (o.toString().trim().equals("")) {
                return true;
            }
        } else if (o instanceof List) {
            if (((List) o).size() == 0) {
                return true;
            }
        } else if (o instanceof Map) {
            if (((Map) o).size() == 0) {
                return true;
            }
        } else if (o instanceof Set) {
            if (((Set) o).size() == 0) {
                return true;
            }
        } else if (o instanceof Object[]) {
            if (((Object[]) o).length == 0) {
                return true;
            }
        } else if (o instanceof int[]) {
            if (((int[]) o).length == 0) {
                return true;
            }
        } else if (o instanceof long[]) {
            if (((long[]) o).length == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取基本数据类型的默认值 包含String和void
     *
     * @param clazz 数据类型
     * @return 对应的默认值
     */
    public static Object primitiveDefaultValue(final Class<?> clazz) {
        if (!BeanKit.isPrimitive(clazz)) throw new ToolBoxException("类型【{}】不是基本数据类型", clazz);

        Map<Class<?>, Object> primitiveDefaultValueMap = BasicType.primitiveDefaultValueMap;
        if (primitiveDefaultValueMap.containsKey(clazz))
            return primitiveDefaultValueMap.get(clazz);
        return BasicType.wrapperPrimitiveDefaultValueMap.get(clazz);


    }

    /**
     * 根据对象类型获取对象的默认值
     *
     * @param clazz 对象类型
     * @return 默认值
     */
    public static Object getDefaultValue(final Class<?> clazz) {
        if (BeanKit.isPrimitive(clazz)) {
            return primitiveDefaultValue(clazz);
        }
        return ClassKit.newInstanceConstructorsDefaultValue(clazz);
    }

}
