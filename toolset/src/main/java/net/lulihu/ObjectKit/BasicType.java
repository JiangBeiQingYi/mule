package net.lulihu.ObjectKit;

import java.util.HashMap;
import java.util.Map;

/**
 * 基本变量类型的枚举
 */
public enum BasicType {
    BYTE, SHORT, INT, INTEGER, LONG, DOUBLE, FLOAT, BOOLEAN, CHAR, CHARACTER;

    /**
     * 原始类型为Key，包装类型为Value，例如： int.class -> Integer.class.
     */
    public static final Map<Class<?>, Class<?>> wrapperPrimitiveMap = new HashMap<>(8);

    /**
     * 包装类型为Key，原始类型为Value，例如： Integer.class -> int.class.
     */
    public static final Map<Class<?>, Class<?>> primitiveWrapperMap = new HashMap<>(8);

    /**
     * 原始数值类型类型为Key，包装类型为Value，例如： int.class -> Integer.class.
     */
    public static final Map<Class<?>, Class<?>> numberWrapperPrimitiveMap = new HashMap<>(6);
    /**
     * 原始数值类型包装类型为Key，原始类型为Value，例如： Integer.class -> int.class.
     */
    public static final Map<Class<?>, Class<?>> numberPrimitiveWrapperMap = new HashMap<>(6);

    /**
     * 原始类型为Key，默认值为value
     */
    public static final Map<Class<?>, Object> primitiveDefaultValueMap = new HashMap<>(8);

    /**
     * 包装类型为Key，默认值为value
     */
    public static final Map<Class<?>, Object> wrapperPrimitiveDefaultValueMap = new HashMap<>(16);


    static {
        // 所有基本数据类型
        wrapperPrimitiveMap.put(Boolean.class, boolean.class);
        wrapperPrimitiveMap.put(Byte.class, byte.class);
        wrapperPrimitiveMap.put(Character.class, char.class);
        wrapperPrimitiveMap.put(Double.class, double.class);
        wrapperPrimitiveMap.put(Float.class, float.class);
        wrapperPrimitiveMap.put(Integer.class, int.class);
        wrapperPrimitiveMap.put(Long.class, long.class);
        wrapperPrimitiveMap.put(Short.class, short.class);
        for (Map.Entry<Class<?>, Class<?>> entry : wrapperPrimitiveMap.entrySet()) {
            primitiveWrapperMap.put(entry.getValue(), entry.getKey());
        }

        // 数值数据类型
        numberWrapperPrimitiveMap.put(Byte.class, byte.class);
        numberWrapperPrimitiveMap.put(Double.class, double.class);
        numberWrapperPrimitiveMap.put(Float.class, float.class);
        numberWrapperPrimitiveMap.put(Integer.class, int.class);
        numberWrapperPrimitiveMap.put(Long.class, long.class);
        numberWrapperPrimitiveMap.put(Short.class, short.class);
        for (Map.Entry<Class<?>, Class<?>> entry : numberWrapperPrimitiveMap.entrySet()) {
            numberPrimitiveWrapperMap.put(entry.getValue(), entry.getKey());
        }

        // 基本数据类型已经其包装类型默认
        primitiveDefaultValueMap.put(boolean.class, false);
        wrapperPrimitiveDefaultValueMap.put(Boolean.class, false);
        primitiveDefaultValueMap.put(byte.class, 0);
        wrapperPrimitiveDefaultValueMap.put(Byte.class, 0);
        primitiveDefaultValueMap.put(short.class, 0);
        wrapperPrimitiveDefaultValueMap.put(Short.class, 0);
        primitiveDefaultValueMap.put(int.class, 0);
        wrapperPrimitiveDefaultValueMap.put(Integer.class, 0);
        primitiveDefaultValueMap.put(long.class, 0);
        wrapperPrimitiveDefaultValueMap.put(Long.class, 0);
        primitiveDefaultValueMap.put(float.class, 0f);
        wrapperPrimitiveDefaultValueMap.put(Float.class, 0f);
        primitiveDefaultValueMap.put(double.class, 0d);
        wrapperPrimitiveDefaultValueMap.put(Double.class, 0d);
        primitiveDefaultValueMap.put(char.class, '\u0000');
        wrapperPrimitiveDefaultValueMap.put(Character.class, '\u0000');

        // 特殊类型
        wrapperPrimitiveDefaultValueMap.put(String.class, "");
        wrapperPrimitiveDefaultValueMap.put(Void.TYPE, null);
    }
}
