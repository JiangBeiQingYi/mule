package net.lulihu.ObjectKit;

import net.lulihu.ToolUtil;

/**
 * 当前线程副本工具
 */
@SuppressWarnings("unchecked")
public class ThreadLocalKit<T> {

    private final ConcurrentThreadLocalKit concurrentThreadLocalKit;

    private final String key;

    public ThreadLocalKit() {
        this.concurrentThreadLocalKit = ConcurrentThreadLocalKit.getInstance();
        // 添加一个空值，先占有key
        this.key = getKey();
        this.concurrentThreadLocalKit.set(this.key, null);
    }

    /**
     * 获取当前线程唯一的key
     *
     * @return 当前线程唯一的key
     */
    private String getKey() {
        String key = ToolUtil.getRandomString(5);
        if (this.concurrentThreadLocalKit.containsKey(key))
            return getKey();
        return key;
    }

    /**
     * 添加副本信息
     *
     * @param t 指定类型的
     */
    public void set(T t) {
        this.concurrentThreadLocalKit.set(this.key, t);
    }

    /**
     * 获取副本信息
     *
     * @return 副本信息
     */
    public T get() {
        return (T) this.concurrentThreadLocalKit.get(this.key);
    }

    public void clear() {
        this.concurrentThreadLocalKit.clear(this.key);
    }

    /**
     * 清除当前线程的所有副本信息
     */
    public static void clearAll() {
        ConcurrentThreadLocalKit.getInstance().clear();
    }

}
