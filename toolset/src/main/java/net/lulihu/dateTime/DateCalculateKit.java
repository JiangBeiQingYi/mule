package net.lulihu.dateTime;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.CollectionKit;
import net.lulihu.ObjectKit.StrKit;
import net.lulihu.exception.ToolBoxException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 时间计算工具类
 */
@Slf4j
public class DateCalculateKit {

    /**
     * 寻找最大时间，并且转换当天的时间
     * <p>
     * 传入的时间转换为 HH:mm
     *
     * @param times 多个时间
     */
    public static DateTime findBiggestTime(List<String> times) {
        String biggestTime = findBiggestTime(times, "HH:mm");
        String format = DateTimeKit.format(DateTimeKit.date(), "HH:mm:00 yyyy-MM-dd");
        return DateTimeKit.parse(biggestTime + format.substring(5), "HH:mm:ss yyyy-MM-dd");
    }

    /**
     * 寻找最大的时间
     *
     * @param times   多个时间
     * @param pattern 日期转换表达式
     */
    public static String findBiggestTime(List<String> times, String pattern) {
        if (CollectionKit.isEmpty(times) || StrKit.isEmpty(pattern)) return null;

        if (times.size() == 1) {
            return times.get(0);
        }
        times.sort((o1, o2) -> compareDate(DateTimeKit.parse(o2, pattern), DateTimeKit.parse(o1, pattern)));
        return times.get(0);
    }

    /**
     * 判断时间是否在时间段内
     *
     * @param nowTime   当前时间
     * @param beginTime 开始时间
     * @param endTime   结束时间
     * @param format    时间格式
     */
    public static boolean intervalJudgment(String nowTime, String beginTime, String endTime, String format) {
        return intervalJudgment(
                DateTimeKit.parse(nowTime, format),
                DateTimeKit.parse(beginTime, format),
                DateTimeKit.parse(endTime, format)
        );
    }


    /**
     * 判断时间是否在时间段内
     *
     * @param nowTime   当前时间
     * @param beginTime 开始时间
     * @param endTime   结束时间
     */
    public static boolean intervalJudgment(Date nowTime, Date beginTime, Date endTime) {
        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        return date.after(begin) && date.before(end);
    }

    /**
     * 时间转cron表达式
     * 小于10的数字之前的0将会被去除
     *
     * @param date 时间对象
     */
    public static String dateToCron(Date date) {
        String format1 = DateTimeKit.format(date, "ss mm HH dd MM");
        String format2 = DateTimeKit.format(date, "? yyyy");
        StringBuilder builder = new StringBuilder();
        for (String str : format1.split(" ")) {
            Integer value = Integer.valueOf(str);
            builder.append(value).append(" ");
        }
        builder.append(format2);
        return builder.toString();
    }

    /**
     * 比较两个时间大小，<br>
     * DATE1大于DATE2 返回1 <br>
     * DATE1小于DATE2 返回-1 <br>
     * 相等返回 0 <br>
     */
    public static int compareDate(Date DATE1, Date DATE2) {
        return Long.compare(DATE1.getTime(), DATE2.getTime());
    }

    /**
     * 计算两个日期相差年数
     *
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @return
     */
    public static int yearDateDiff(String startDate, String endDate) {
        Calendar calBegin = Calendar.getInstance();
        Calendar calEnd = Calendar.getInstance();
        calBegin.setTime(DateTimeKit.parse(startDate, "yyyy"));
        calEnd.setTime(DateTimeKit.parse(endDate, "yyyy"));
        return calEnd.get(Calendar.YEAR) - calBegin.get(Calendar.YEAR);
    }

    /**
     * 获取指定时间周的第一天与最后一天
     * <p>
     * 一周的第一天为 星期日，最后一天为星期六
     *
     * @param time 指定时间
     * @return 返回一个长度为2的数组
     */
    public static String[] weekStartAndEnd(Date time) {
        SimpleDateFormat dateFormat = DateTimeKit.NORM_DATE_FORMAT.get();
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        // 获得当前日期是一个星期的第几天
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
        // 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        // 设置一个星期的第一天，为星期日
        cal.setFirstDayOfWeek(Calendar.SUNDAY);
        // 根据日历的规则，给指定日期减去星期几与一个星期第一天的差值
        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - cal.get(Calendar.DAY_OF_WEEK));
        String imptimeBegin = dateFormat.format(cal.getTime());
        cal.add(Calendar.DATE, 6);
        String imptimeEnd = dateFormat.format(cal.getTime());
        return new String[]{imptimeBegin, imptimeEnd};
    }

    /**
     * 获取某一时间段内每周指定星期的日期
     *
     * @param dateFrom 开始时间
     * @param dateEnd  结束时间
     * @param weekDays 星期 多个用 | 隔开
     * @return 返回时间数组  每组时间为一个字符串
     */
    public static List<String> weekDays(String dateFrom, String dateEnd, String weekDays, String separator) {
        //得到对应星期的系数
        String[] strWeeks = weekDays.split("\\|");
        String strWeekNumber = weekForNum(strWeeks);
        int len = strWeeks.length;

        List<String> result = new ArrayList<>();

        SimpleDateFormat dateFormat = DateTimeKit.NORM_DATE_FORMAT.get();
        dateFrom = dateFormat.format(
                DateTimeKit.parse(dateFrom).getTime() - DateTimeKit.DAY_MS);
        StringBuilder builder = new StringBuilder();
        while (true) {
            Date date = new Date(DateTimeKit.parse(dateFrom).getTime() + DateTimeKit.DAY_MS);
            dateFrom = dateFormat.format(date);
            if (dateFrom.compareTo(dateEnd) <= 0) {
                //查询的某一时间的星期系数
                Integer weekDay = dayForWeek(date);
                //判断当期日期的星期系数是否是需要查询的
                if (strWeekNumber.contains(weekDay.toString())) {
                    builder.append(dateFrom).append(separator);
                    if (len == 0 || weekDay == 7) {
                        result.add(builder.substring(0, builder.length() - separator.length()));
                        builder = new StringBuilder();
                        len = strWeeks.length;
                    }
                    len--;
                }
            } else break;
        }
        return result;
    }

    /**
     * 得到对应星期的系数  星期日：1，星期一：2，星期二：3，星期三：4，星期四：5，星期五：6，星期六：7
     *
     * @param strWeeks 星期格式
     */
    public static String weekForNum(String... strWeeks) {
        StringBuilder weekNumber = new StringBuilder();
        for (String strWeek : strWeeks) {
            weekNumber.append(getWeekNum(strWeek));
        }
        return weekNumber.toString();

    }

    /**
     * 等到当期时间的周系数
     *
     * @param date 星期日：1，星期一：2，星期二：3，星期三：4，星期四：5，星期五：6，星期六：7
     * @return
     */

    public static Integer dayForWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * 将星期转换为对应的系数
     *
     * @param strWeek 星期日：1，
     *                星期一：2，
     *                星期二：3，
     *                星期三：4，
     *                星期四：5，
     *                星期五：6，
     *                星期六：7
     * @return 对应系数
     */
    public static Integer getWeekNum(String strWeek) {
        if ("星期日".equals(strWeek)) {
            return 1;
        } else if ("星期一".equals(strWeek)) {
            return 2;
        } else if ("星期二".equals(strWeek)) {
            return 3;
        } else if ("星期三".equals(strWeek)) {
            return 4;
        } else if ("星期四".equals(strWeek)) {
            return 5;
        } else if ("星期五".equals(strWeek)) {
            return 6;
        } else if ("星期六".equals(strWeek)) {
            return 7;
        }
        throw new ToolBoxException("不支持的星期书写方式");
    }


}
