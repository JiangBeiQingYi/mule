package net.lulihu.dataStructure;

import net.lulihu.ObjectKit.ReflectKit;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 基于LinkedHashMap实现的线程安全的LRULinkedHashMap
 *
 * @param <K>
 * @param <V>
 */
public class LRULinkedHashMap<K, V> extends LinkedHashMap<K, V> {

    private final Lock readLock;
    private final Lock writeLock;

    public LRULinkedHashMap() {
        super();
        //读写锁
        ReadWriteLock globalLock = new ReentrantReadWriteLock();
        readLock = globalLock.readLock();
        writeLock = globalLock.writeLock();
    }

    @Override
    public V get(Object key) {
        try {
            readLock.lock();//先加读锁
            return super.get(key);
        } finally {
            readLock.unlock();//解锁
        }
    }

    @Override
    public V put(K key, V value) {
        try {
            writeLock.lock();//先加写锁
            return super.put(key, value);
        } finally {
            writeLock.unlock();//解锁
        }
    }

    /**
     * 获取首个元素
     */
    public Map.Entry<K, V> getHead() {
        try {
            readLock.lock();//先加读锁
            return entrySet().iterator().next();
        } finally {
            readLock.unlock();//解锁
        }
    }

    /**
     * 获取最后一个元素
     * 时间复杂度O(n)
     */
    public Map.Entry<K, V> getTail() {
        try {
            readLock.lock();//先加读锁
            Iterator<Map.Entry<K, V>> iterator = entrySet().iterator();
            Map.Entry<K, V> tail = null;
            while (iterator.hasNext()) {
                tail = iterator.next();
            }
            return tail;
        } finally {
            readLock.unlock();//解锁
        }
    }

    /**
     * 根据反射获取最后一个元素
     * 时间复杂度O(1)，访问tail属性
     *
     * @return 最后一个元素
     */
    @SuppressWarnings("unchecked")
    public Map.Entry<K, V> getTailByReflection() throws IllegalAccessException {
        try {
            readLock.lock();//先加读锁
            return (Map.Entry<K, V>) ReflectKit.getFieldValue(this, "tail");
        } finally {
            readLock.unlock();//解锁
        }
    }
}