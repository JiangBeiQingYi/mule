package net.lulihu.exception;

import lombok.Getter;
import net.lulihu.ObjectKit.StrKit;

/**
 * 请求异常
 */
public class RequestException extends RuntimeException {

    @Getter
    private Integer responseCode;

    @Getter
    private String responseMessage;

    @Getter
    private String responseResult;


    public RequestException(String message) {
        super(message);
    }

    public RequestException(String message, Object... values) {
        super(StrKit.format(message, values));
    }

    public RequestException(Integer responseCode, String responseMessage) {
        super(responseCode + " : " + responseMessage);
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public RequestException(Integer responseCode, String responseMessage, String responseResult) {
        super(StrKit.format("错误代码:{}, 错误消息:{}, 响应结果: {}", responseCode, responseMessage, responseResult));
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.responseResult = responseResult;
    }

    public RequestException(Throwable cause) {
        super(cause);
    }

    public RequestException(String message, Throwable cause) {
        super(message, cause);
    }


}
