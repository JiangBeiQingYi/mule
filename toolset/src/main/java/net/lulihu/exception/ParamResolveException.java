package net.lulihu.exception;

import net.lulihu.ObjectKit.StrKit;

/**
 * 参数解析异常
 */
public class ParamResolveException extends RuntimeException implements ExceptionEnum {

    private String message = "参数不合法";

    private Integer code;

    private Integer httpCode;

    /**
     * 默认http 错误代码
     */
    private static final Integer defaultHttpCode = 400;

    /**
     * 默认自定义错误消息代码
     */
    private static final Integer defaultCode = 1005;

    public ParamResolveException() {
        super();
    }

    public ParamResolveException(ExceptionEnum exceptionEnum) {
        this(exceptionEnum.getHttpCode(), exceptionEnum.getCode(), exceptionEnum.getMessage());
    }

    public ParamResolveException(Integer code, String message, Object... values) {
        this(defaultHttpCode, code, StrKit.format(message, values));
    }

    public ParamResolveException(String message, Object... values) {
        this(defaultHttpCode, defaultCode, StrKit.format(message, values));
    }

    public ParamResolveException(Integer httpCode, Integer code, String message) {
        super(message);
        this.message = message;
        this.httpCode = httpCode;
        this.code = code;
    }


    @Override
    public Integer getHttpCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Integer getCode() {
        return httpCode;
    }
}
