package net.lulihu.exception;

/**
 * 异常枚举接口
 */
public interface ExceptionEnum {

    /**
     * 获取异常编码
     */
    Integer getHttpCode();

    /**
     * 获取异常编码
     */
    Integer getCode();

    /**
     * 获取异常信息
     */
    String getMessage();

}