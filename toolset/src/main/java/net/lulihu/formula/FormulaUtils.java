package net.lulihu.formula;

import net.lulihu.ObjectKit.StrKit;

/**
 * 解析表达式运算
 */
public class FormulaUtils {

    /**
     * 表达式运算
     *
     * @param expression 要计算的表达式,如"1+2+3+4"
     * @return 返回计算结果, 如果带有逻辑运算符则返回true/false,否则返回数值
     */
    public static Object eval(String expression) {
        return ExpressionEvaluator.eval(expression);
    }

    /**
     * 逻辑表达式运算
     *
     * @param expression 要计算的表达式,如 20<50&&76<90 或者 1+2+3=6
     * @return 返回计算结果,  返回true 或者 false
     */
    public static boolean logicEval(String expression) {
        Object eval = eval(expression);
        if (eval instanceof Boolean)
            return (boolean) eval;
        throw new ExpressionException(StrKit.format("错误的逻辑表达式:{}", expression));
    }

    /**
     * 评估三元运算
     *
     * @param expression 要计算的表达式
     */
    public static Object evalThreeOperand(String expression) {
        return ExpressionEvaluator.evalThreeOperand(expression);
    }


}