package net.lulihu.formula;

/**
 * 表达式异常类
 */
public class ExpressionException extends RuntimeException {


    public ExpressionException() {
        super();
    }

    public ExpressionException(String msg) {
        super(msg);
    }

    public ExpressionException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ExpressionException(Throwable cause) {
        super(cause);
    }
}